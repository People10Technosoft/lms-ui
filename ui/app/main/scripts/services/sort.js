'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.sort
 * @description
 * # sort
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
    .service('sort', function() {
        // AngularJS will instantiate a singleton by calling "new" on this function
        this.sortLeave = function(employee) {
            var records = [];
            var counter;


            var len = employee.length;
            if (len > 0) {
                var header = {
                        title: "Pending",
                        divider: true,
                        count: 0
                    }
                    //pending
                records.push(header);
                for (var i = 0; i < len; i++) {
                    if (employee[i].status === "Pending") {
                        // console.log($scope.employee[i].status);
                        employee[i].expanded = false;
                        employee[i].color = " #fe9304";
                        employee[i].fromDate = new Date(employee[i].fromDate);
                        employee[i].toDate = new Date(employee[i].toDate);
                        records.push(employee[i]);
                        records[0].count++;
                    }
                }
                counter = records[0].count + 1;
                len = employee.length;
                //Future
                if (len > 0) {
                    header = {
                        title: "Future",
                        divider: true,
                        count: 0

                    }
                    records.push(header);
                    var currentDate = new Date();

                    for (var i = 0; i < len; i++) {
                        if ((employee[i].status === "Approved") && (new Date(employee[i].fromDate) > currentDate)) {
                            employee[i].expanded = false;
                            employee[i].color = "#4caf50";
                            employee[i].fromDate = new Date(employee[i].fromDate);
                            employee[i].toDate = new Date(employee[i].toDate);
                            records.push(employee[i]);
                            records[counter].count++;

                        }
                    }
                    counter = counter + records[counter].count + 1;
                    len = employee.length;
                } //inner if statement
                if (len > 0) {
                    header = {
                        title: "Taken",
                        divider: true,
                        count: 0
                    }
                    records.push(header);
                    var currentDate = new Date();
                    //Taken
                    for (var i = 0; i < len; i++) {
                        if ((employee[i].status === "Approved") && (new Date(employee[i].fromDate) < currentDate)) {
                            employee[i].expanded = false;
                            employee[i].color = "#bdbdbd";
                            employee[i].fromDate = new Date(employee[i].fromDate);
                            employee[i].toDate = new Date(employee[i].toDate);
                            records.push(employee[i]);
                            records[counter].count++;

                        }
                    }
                }
            } //end of if statement

            return records;

        };
    });
