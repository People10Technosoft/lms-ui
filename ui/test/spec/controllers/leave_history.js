'use strict';

describe('Controller: LeaveHistoryCtrl', function () {

  // load the controller's module
  beforeEach(module('yoErpLmsApp'));

  var LeaveHistoryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LeaveHistoryCtrl = $controller('LeaveHistoryCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LeaveHistoryCtrl.awesomeThings.length).toBe(3);
  });
});
