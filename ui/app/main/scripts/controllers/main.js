'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('MainCtrl', ['$scope', '$rootScope', '$window', '$location', '$filter', '$http', '$q', '$localStorage', 'sweet', '$route', 'dialogMessage', '$mdDialog', '$mdSidenav', 'commonData', '$log', '$timeout', '$mdMedia',
        function($scope, $rootScope, $window, $location, $filter, $http, $q, $localStorage, sweet, $route, dialogMessage, $mdDialog, $mdSidenav, commonData, $log, $timeout, $mdMedia) {
            $rootScope.nodeUrl = "http://10.0.1.112:5000/";
            $rootScope.gruntUrl = "10.0.1.112";
            $scope.sideNavShown = true;
            $scope.showTooltip = false;


            // Observe screen change
            commonData.observeScreen().then(null, null, function(data) {
                $scope.screen = data;
            });

            // Sidebar for mobile and desktop view
            if ($mdMedia('gt-md')) {
                $scope.sideNavShown = true;
            } else {
                $scope.sideNavShown = false;
            }

            // Identifying active screen
            if ($location.path() === '/approvals') {
                $rootScope.view = "Approvals";
                $scope.screen = "Approvals";

            } else if ($location.path() === '/my_team') {
                $rootScope.view = "Approvals";
                $scope.screen = "MyTeam";
            } else if ($location.path() === '/leave_history') {
                $rootScope.view = "MySpace";
                $scope.screen = "MySpace";
            } else if ($location.path() === '/admin_space') {
                $rootScope.view = "Approvals";
                $scope.screen = "AdminSpace";
            } else if ($location.path() === '/user_detail') {
                $rootScope.view = "Approvals";
                $scope.screen = "MyProfile";
            } else {
                $rootScope.view = "Others";
                $scope.screen = "Others";
            }

            // Refresh and redirecting to leave history page
            $scope.refresh = function() {
                $rootScope.view = "MySpace";
                window.location.href = "#/leave_history";
                $window.location.reload(true);
            }

            // toggle side bar
            $scope.toggleMenu = function() {
                $scope.sideNavShown = !$scope.sideNavShown;
                if ($scope.sideNavShown) {} else {}
            };

            // logout function
            $scope.logoutUser = function(token) {
                $http({
                        url: $rootScope.nodeUrl + "api/employees/logout?access_token=" + $rootScope.globals.currentUser.token,
                        method: "POST"
                    })
                    .then(function(response) {
                            // success
                            $mdDialog.cancel();
                            dialogMessage.showErrorToast('You are logged out successfully');
                            $rootScope.globals = {};
                            window.sessionStorage.clear();
                            $location.path('/login');
                            $route.reload(true);
                        },
                        function(response) { // optional
                            // failed
                            dialogMessage.showErrorToast(response.data.error.message);
                        });

            }


            /*subheader*/
            $scope.changeScreen = function(scr) {
                if (scr == "Approvals") {
                    $rootScope.view = "Approvals";
                    $scope.screen = "Approvals";
                    window.location.href = "#/approvals";

                } else if (scr == "MySpace") {
                    $rootScope.view = "MySpace";
                    $scope.screen = "MySpace";
                    window.location.href = "#/leave_history";

                } else if (scr == "MyTeam") {
                    $rootScope.view = "Approvals";
                    $scope.screen = "MyTeam";
                    window.location.href = "#/my_team";
                } else if (scr == "AdminSpace") {
                    $rootScope.view = "Approvals";
                    $scope.screen = "AdminSpace";
                    window.location.href = "#/admin_space";
                } else if (scr == "MyProfile") {
                    $rootScope.view = "Approvals";
                    $scope.screen = "MyProfile";
                    window.location.href = "#/user_detail";
                } else {
                    $rootScope.view = "Others";
                    $scope.screen = "Others";
                    window.location.href = "#/leave_history";
                }
                if ($mdMedia('xs') || $mdMedia('sm')) {
                    $scope.sideNavShown = false;
                }
            }
            $scope.refresh = function() {
                window.location.href = "#/leave_history";
                $window.location.reload(true);
            }

            // Open Add employee view for administration page
            $scope.openAddEmployeeDialog = function(message) {
                var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;

                $mdDialog.show({
                    controller: 'AddNewEmployeeCtrl',
                    templateUrl: 'administration/views/add_new_employee.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    hasBackdrop: false,
                    locals: {
                        message: message
                    }

                })

            }
        }
    ]);
