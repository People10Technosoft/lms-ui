'use strict';

describe('Service: colorSelector', function () {

  // load the service's module
  beforeEach(module('yoErpLmsApp'));

  // instantiate service
  var colorSelector;
  beforeEach(inject(function (_colorSelector_) {
    colorSelector = _colorSelector_;
  }));

  it('should do something', function () {
    expect(!!colorSelector).toBe(true);
  });

});
