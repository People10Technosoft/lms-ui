angular.module('yoErpLmsApp')
    .directive('managerDetail', function() {
        return {
            restrict: 'E',
            templateUrl: 'main/scripts/directives/manager_detail/manager_detail.html',
            scope: {
                options: "=",
                record: "="
            }
        };
    });
