'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:UserDetailCtrl
 * @description
 * # UserDetailCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('UserDetailCtrl', ['$scope', '$rootScope', '$http', 'webService', '$log', 'commonData',
        function($scope, $rootScope, $http, webService, $log, commonData) {
            commonData.setScreen("MyProfile");
            $rootScope.view = "UserDetail";
            var url1 = $rootScope.nodeUrl + "api/employees/profileInformation?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(url1).success(function(data) {
                $scope.user = data;
            });
            var tabs = [{
                title: 'About',
                content: "main/views/about.html"
            }];
            var selected = null,
                previous = null;
            $scope.tabs = tabs;
            $scope.selectedIndex = 0;
            $scope.$watch('selectedIndex', function(current, old) {
                previous = selected;
                selected = tabs[current];
            });

        }
    ]);
