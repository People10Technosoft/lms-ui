'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:MemberSpaceCtrl
 * @description
 * # MemberSpaceCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('MemberSpaceCtrl', ['$scope', '$routeParams', 'webService', '$rootScope', 'commonData', function($scope, $routeParams, webService, $rootScope, commonData) {
        $rootScope.view = "Others";
        $scope.colors = commonData.getLeaveColor();
        $scope.id = $routeParams.id;
        $rootScope.e_id = $routeParams.id;
        $rootScope.index = $routeParams.index;
        $rootScope.group = $routeParams.title;
        $rootScope.lvId = $routeParams.leaveId;
        $scope.leave = [];
        $scope.tooltip = commonData.getTooltip();

        var tabs = [{
            title: 'Leave Summary',
            content: "employee/views/emp_leave_summary.html"
        }, ];
        var selected = null,
            previous = null;
        $scope.tabs = tabs;
        $scope.selectedIndex = 0;
        $scope.$watch('selectedIndex', function(current, old) {
            previous = selected;
            selected = tabs[current];
        });

        var url = $rootScope.nodeUrl + "api/employees/profileInformation?id=" + $scope.id + "&access_token=" + $rootScope.globals.currentUser.token;
        webService.getSummary(url).success(function(data) {
            $scope.user = data;
        });

        var url1 = $rootScope.nodeUrl + "api/leave_requests/leaveStatistics?id=" + $scope.id + "&access_token=" + $rootScope.globals.currentUser.token;
        webService.getSummary(url1).success(function(data) {
            $scope.leave = data;
        });
        var url2 = $rootScope.nodeUrl + "api/leave_requests/getList?id=" + $scope.id + "&access_token=" + $rootScope.globals.currentUser.token;
        webService.getSummary(url2).success(function(data) {
            $scope.history = data;
        });
        $scope.leaveDetails = function(event, index, title, leaveId) {
            event.stopPropagation();
            $scope.selectedUserIndex = index;
            $scope.title = title;
            var url3 = $rootScope.nodeUrl + "api/leave_workflows/getLeaveWorkflows?leaveId=" + leaveId + "&employeeId=" + $scope.id + "&access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(url3).success(function(data) {
                $scope.workflow = data;
                var index = $scope.workflow.length - 1;
                $rootScope.employeeApprover = $scope.workflow[index].toEmployee;
            });

        };

        $scope.listItemClick = function($index) {
            var clickedItem = $scope.items[$index];
        };
        $scope.selectedUserIndex = undefined;
        $scope.title = undefined;
        $scope.selectUserIndex = function(index, title) {
            if ($scope.selectedUserIndex !== index || $scope.title !== title) {
                $scope.selectedUserIndex = index;
                $scope.title = title;
            } else {
                $scope.selectedUserIndex = undefined;
                $scope.title = undefined;
            }
        };


    }]);
