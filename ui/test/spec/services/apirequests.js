'use strict';

describe('Service: apiRequests', function () {

  // load the service's module
  beforeEach(module('yoErpLmsApp'));

  // instantiate service
  var apiRequests;
  beforeEach(inject(function (_apiRequests_) {
    apiRequests = _apiRequests_;
  }));

  it('should do something', function () {
    expect(!!apiRequests).toBe(true);
  });

});
