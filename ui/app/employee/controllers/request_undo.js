'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:RequestUndoCtrl
 * @description
 * # RequestUndoCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('RequestUndoCtrl', function($scope, $rootScope, $mdDialog, $http, $filter, $localStorage, sort, commonData, leaveType, dialogMessage, sweet, $route, $mdToast, message, webService) {
        $scope.leaveType = $rootScope.user;
        $scope.user = message;
        if ($scope.user.numberOfDays == 0.5) {

            $scope.halfDay = true;
        }
        $scope.user.numberOfDays;
        var paternity_count;
        var maternity_count;
        commonData.observeLeaves().then(null, null, function(data) {
            $scope.leaves = data;
            angular.forEach($scope.leaves, function(obj) {
                if (obj.name === "Paternity") {
                    paternity_count = obj.count;

                } else if (obj.name === "Maternity") {
                    maternity_count = obj.count;
                }
            });
        });

        $http.get($rootScope.nodeUrl + "specialLeaves?id=" + $rootScope.globals.currentUser.empId)
            .then(function(response) {

                commonData.setLeaves(leaveType.leaveItems(response.data));
                $scope.leaves = commonData.getLeaves();
                angular.forEach($scope.leaves, function(obj) {
                    if (obj.name === "Paternity") {
                        paternity_count = obj.count;

                    } else if (obj.name === "Maternity") {
                        maternity_count = obj.count;
                    }
                });
            });
        $http.get($rootScope.nodeUrl + "api/employees/getApprovars?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
            .then(function(response) {

                $scope.managers = response.data;
                $scope.approverId = $scope.managers[0].name.toUpperCase();

                $scope.user.approverId = $scope.managers[0].id;
            });


        $scope.selectApprover = function(id) {
            $scope.user.approverId = id;
        }
        $scope.user.employeeId = "" + $rootScope.globals.currentUser.empId;

        $scope.closeDialog = function() {
            $mdDialog.cancel();
        };
        $scope.halfDayCalc = function() {
            $scope.user.toDate = $scope.user.fromDate;
            if ($scope.halfDay) {
                $scope.user.numberOfDays = 0.5;
                $scope.user.halfDaySlot = 1;
            } else {
                $scope.user.numberOfDays = 1;


            }
        }
        $scope.selectToDate = function() {
            var fromDate = $filter('date')($scope.user.fromDate, 'yyyy-MM-dd');
            var days = $scope.user.numberOfDays;
            if ($scope.user.type === "Paternity" || $scope.user.type === "Maternity") {
                $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&numberOfDays=" + days)
                    .then(function(response) {
                        $scope.user.toDate = new Date(response.data.toDate);
                    });
            } else {
                $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&numberOfDays=" + $scope.user.numberOfDays)
                    .then(function(response) {
                        $scope.user.toDate = new Date(response.data.toDate);
                    });
            }
        }
        $scope.to_datePicker = function() {
            var fromDate = $filter('date')($scope.user.fromDate, 'yyyy-MM-dd');
            var days = $scope.user.numberOfDays;
            if (days !== 0.5) {
                var parsed = parseFloat(days, 10);
                if (parsed !== parsed) {
                    return null;
                } // check for NaN
                $scope.user.numberOfDays = Math.round(parsed);
                days = $scope.user.numberOfDays;
            }
            angular.forEach($scope.leaves, function(obj) {
                if ((obj.name === $scope.user.type) && (obj.count < days)) {
                    if (obj.name !== "Loss of Pay") {
                        $scope.user.numberOfDays = obj.count;
                    }
                }
            });
            if ($scope.user.type === "Paternity" || $scope.user.type === "Maternity") {
                $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&numberOfDays=" + days)
                    .then(function(response) {
                        $scope.user.toDate = new Date(response.data.toDate);
                    });
            } else {
                $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&numberOfDays=" + days)
                    .then(function(response) {
                        $scope.user.toDate = new Date(response.data.toDate);
                        if (response.data.halfDay === 1) {

                            $scope.halfDay = true;
                        }
                    });
            }
        }
        $scope.special_to_date = function() {
            var fromDate = $filter('date')($scope.user.fromDate, 'yyyy-MM-dd');
            angular.forEach($scope.leaves, function(obj) {
                if ((obj.name === $scope.user.type) && (obj.count < $scope.user.numberOfDays)) {
                    if (obj.name !== "Loss of Pay") {
                        $scope.user.numberOfDays = obj.count;
                    }
                }
            });
            if ($scope.user.type === "Paternity") {
                $scope.user.numberOfDays = paternity_count;
                $scope.user.description = "Paternity Leave";
                $scope.user.halfDaySlot = 1;
                $scope.halfDay = false;
                $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&numberOfDays=" + $scope.user.numberOfDays)
                    .then(function(response) {
                        $scope.user.toDate = new Date(response.data.toDate);
                    });
            } else if ($scope.user.type === "Maternity") {
                $scope.user.numberOfDays = maternity_count;
                $scope.user.description = "Maternity Leave";
                $scope.user.halfDaySlot = 1;
                $scope.halfDay = false;
                $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&numberOfDays=" + $scope.user.numberOfDays)
                    .then(function(response) {
                        $scope.user.toDate = new Date(response.data.toDate);
                    });
            } else {
                $scope.user.toDate = new Date($scope.user.toDate);
                var toDate = $filter('date')($scope.user.toDate, 'yyyy-MM-dd');
                if ($scope.user.description === "Paternity Leave" || $scope.user.description === "Maternity Leave") {
                    $scope.user.description = "";
                }
                $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&numberOfDays=" + $scope.user.numberOfDays)
                    .then(function(response) {
                        $scope.user.toDate = new Date(response.data.toDate);
                        if (response.data.halfDay === 1) {

                            $scope.halfDay = true;
                        }
                    });
            }
        }
        $scope.dayPicker = function() {
            var fromDate = $filter('date')($scope.user.fromDate, 'yyyy-MM-dd');
            var toDate = $filter('date')($scope.user.toDate, 'yyyy-MM-dd');
            if ($scope.user.type === "Paternity" || $scope.user.type === "Maternity") {
                $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&toDate=" + toDate)
                    .then(function(response) {
                        $scope.user.numberOfDays = Number(response.data.numberOfDays);
                        if (($scope.user.type === "Paternity") && ($scope.user.numberOfDays > paternity_count)) {
                            $scope.user.numberOfDays = paternity_count;
                        } else if (($scope.user.type === "Maternity") && ($scope.user.numberOfDays > maternity_count)) {
                            $scope.user.numberOfDays = maternity_count;
                        }
                        $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&numberOfDays=" + $scope.user.numberOfDays)
                            .then(function(response) {
                                $scope.user.toDate = new Date(response.data.toDate);
                            });
                    });
            } else {
                $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&toDate=" + toDate)
                    .then(function(response) {
                        $scope.user.numberOfDays = Number(response.data.numberOfDays);
                        angular.forEach($scope.leaves, function(obj) {
                            if ((obj.name === $scope.user.type) && (obj.count < $scope.user.numberOfDays)) {
                                if (obj.name !== "Loss of Pay") {
                                    $scope.user.numberOfDays = obj.count;
                                    $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&numberOfDays=" + $scope.user.numberOfDays)
                                        .then(function(response) {
                                            $scope.user.toDate = new Date(response.data.toDate);
                                        });
                                }
                            }
                        });
                    });
            }
        }

        $scope.leaveApply = function(validation) {
            if (validation) {
                var message = $scope.user;
                $mdDialog.hide();
                try {

                    var toast = $mdToast.simple()
                        .textContent('Submitting...')
                        .action('UNDO')
                        .highlightAction(false)
                        .position('fixed bottom left')
                        .hideDelay(500);

                    $mdToast.show(toast).then(function(response) {
                        if (response == 'ok') {
                            $mdDialog.show({
                                controller: 'RequestUndoCtrl',
                                templateUrl: 'employee/views/request_undo.html',
                                parent: angular.element(document.body),
                                /*targetEvent: ev,*/
                                clickOutsideToClose: true,
                                fullscreen: false,
                                hasBackdrop: false,
                                fullscreen: false,

                                locals: {
                                    message: message
                                }
                            })
                        } else {

                            if (!$scope.halfDay) {
                                $scope.user.halfDaySlot = 0;
                            }
                            $scope.json = angular.toJson($scope.user);
                            $http({
                                    url: $rootScope.nodeUrl + 'api/leave_requests?access_token=' + $rootScope.globals.currentUser.token,
                                    method: "POST",
                                    data: $scope.json
                                })
                                .then(function(response) {
                                        // success
                                        dialogMessage.showErrorToast('Your leave has been applied successfully');
                                        // $route.reload();
                                        $http.get($rootScope.nodeUrl + "api/leave_requests/leaveStatistics?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                            .then(function(response) {
                                                $rootScope.myLeave = response.data;
                                            });
                                        $http.get($rootScope.nodeUrl + "api/leave_requests/getList?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                            .then(function(response) {

                                                $scope.employee = response.data;
                                                commonData.setHistory(response.data);
                                            });
                                        $http.get($rootScope.nodeUrl + "specialLeaves?id=" + $rootScope.globals.currentUser.empId)
                                            .then(function(response) {
                                                commonData.setLeaves(leaveType.leaveItems(response.data));
                                            });
                                        var API_leaveSummary = $rootScope.nodeUrl + "api/leave_requests/leaveHistorySummary?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token;
                                        webService.getSummary(API_leaveSummary).success(function(data) {
                                            commonData.setEmployeeLeaveSummary(data);
                                        });

                                        $mdDialog.cancel();
                                    },
                                    function(response) { // optional
                                        // failed
                                        dialogMessage.showErrorToast(response.data.error.message);
                                        $mdDialog.show({
                                            controller: 'RequestUndoCtrl',
                                            templateUrl: 'employee/views/request_undo.html',
                                            parent: angular.element(document.body),
                                            /*targetEvent: ev,*/
                                            clickOutsideToClose: true,
                                            fullscreen: false,
                                            disableParentScroll: false,
                                            hasBackdrop: false,
                                            fullscreen: false,

                                            locals: {
                                                message: message
                                            }
                                        })
                                    });

                        }
                    });

                } catch (e) {
                    // console.log("error");
                }
            }
        }
    });
