'use strict';

describe('Service: leaveAction', function () {

  // load the service's module
  beforeEach(module('yoErpLmsApp'));

  // instantiate service
  var leaveAction;
  beforeEach(inject(function (_leaveAction_) {
    leaveAction = _leaveAction_;
  }));

  it('should do something', function () {
    expect(!!leaveAction).toBe(true);
  });

});
