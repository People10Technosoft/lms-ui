'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:MyTeamCtrl
 * @description
 * # MyTeamCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('MyTeamCtrl', ['$scope', '$http', '$rootScope', '$window', '$location', 'commonData', 'webService',
        function($scope, $http, $rootScope, $window, $location, commonData, webService) {
            $scope.selectedUserIndex = undefined;
            $scope.visibleScreen = "MyTeam";
            $scope.progress = true;
            $scope.progress_mode = "indeterminate";

            commonData.setScreen("MyTeam");

            // API for getting team members details
            var url1 = $rootScope.nodeUrl + "api/employees/getTeamMemberDetails?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(url1).success(function(data) {
                $scope.teams = data;
                $scope.progress = false;
                $scope.progress_mode = null;
            });

            // getting color themes
            $scope.colors = commonData.getLeaveColor();

            // redirect to employee's member space
            $scope.memberSpace = function(event, id) {
                event.stopPropagation();
                var url = "/member_space/" + id;
                $location.path(url);
            }

        }
    ]);
