'use strict';

describe('Service: localVariableStore', function () {

  // load the service's module
  beforeEach(module('yoErpLmsApp'));

  // instantiate service
  var localVariableStore;
  beforeEach(inject(function (_localVariableStore_) {
    localVariableStore = _localVariableStore_;
  }));

  it('should do something', function () {
    expect(!!localVariableStore).toBe(true);
  });

});
