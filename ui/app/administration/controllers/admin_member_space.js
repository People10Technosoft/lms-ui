'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:MemberSpaceCtrl
 * @description
 * # MemberSpaceCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('AdminMemberSpaceCtrl', ['$scope', 'webService', '$rootScope', 'commonData',
        function($scope, webService, $rootScope, commonData) {

            $rootScope.view = "Admin view";
            $scope.colors = commonData.getLeaveColor();
            $scope.leave = [];

            // Employee's Leave History

            commonData.observeHistory().then(null, null, function(data) {
                $scope.history = data;
            });

            // Employee's Leave summary

            commonData.observeLeaveSummary().then(null, null, function(data) {
                $scope.leave = data;
            });

            //Expand card with workflow

            $scope.leaveDetails = function(event, index, title, leaveId) {
                event.stopPropagation();
                $scope.selectedUserIndex = index;
                $scope.title = title;
                var url3 = $rootScope.nodeUrl + "api/leave_workflows/getLeaveWorkflows?leaveId=" + leaveId + "&employeeId=" + $scope.id + "&access_token=" + $rootScope.globals.currentUser.token;
                webService.getSummary(url3).success(function(data) {
                    $scope.workflow = data;
                    var index = $scope.workflow.length - 1;
                    $rootScope.employeeApprover = $scope.workflow[index].toEmployee;
                });

            };

            //Collapse the current card
            $scope.selectUserIndex = function(index, title) {
                if ($scope.selectedUserIndex !== index || $scope.title !== title) {
                    $scope.selectedUserIndex = index;
                    $scope.title = title;
                } else {
                    $scope.selectedUserIndex = undefined;
                    $scope.title = undefined;
                }
            };


        }
    ]);
