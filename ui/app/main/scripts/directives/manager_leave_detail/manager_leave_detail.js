angular.module('yoErpLmsApp')
    .directive('managerLeaveDetail', function() {
        return {
            restrict: 'E',
            templateUrl: 'main/scripts/directives/manager_leave_detail/manager_leave_detail.html',
            scope: {
                options: "=",
                content: "=",
                datefilter: "@"
            },
            controller: function($scope, $filter) {

                if ($scope.content.lastLeaveTakenFromDate) {
                    $scope.filteredContent = $filter($scope.datefilter)($scope.content.lastLeaveTakenFromDate) + " - " + $filter($scope.datefilter)($scope.content.lastLeaveTakenToDate);
                } else if ($scope.datefilter != null && !$scope.content.leaveId) {
                    $scope.filteredContent = $filter($scope.datefilter)($scope.content);
                } else if ($scope.datefilter != null) {
                    $scope.filteredContent = "-";
                } else {
                    $scope.filteredContent = $scope.content;
                }


            }
        };
    });
