'use strict';

describe('Controller: MemberSpaceCtrl', function () {

  // load the controller's module
  beforeEach(module('yoErpLmsApp'));

  var MemberSpaceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MemberSpaceCtrl = $controller('MemberSpaceCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MemberSpaceCtrl.awesomeThings.length).toBe(3);
  });
});
