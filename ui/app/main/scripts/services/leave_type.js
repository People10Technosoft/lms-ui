'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.leaveType
 * @description
 * # leaveType
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
    .service('leaveType', function() {

        // type of leave with images color code 
        this.leaveItems = function(leaves) {
            var items = [];
            var privilege = {
                name: "Privilege",
                icon: "images/Privilege.svg",
                color: "#e91e63",
                direction: "left",
                count: 0
            };
            var LOP = {
                name: "Loss of Pay",
                icon: "images/LOP.svg",
                color: "#9C27B0",
                direction: "left",
                count: 0
            };
            var paternity = {
                name: "Paternity",
                icon: "images/Paternity.svg",
                color: "#BA68C8",
                direction: "left",
                count: 0
            };
            var maternity = {
                name: "Maternity",
                icon: "images/Maternity.svg",
                color: "#F06292",
                direction: "left",
                count: 0
            };
            var COMPOFF = {
                name: "Comp-off",
                icon: "images/compoff.svg",
                color: "#4DD0E1",
                direction: "left",
                count: 0
            };
            var plan = {
                name: "Plan",
                icon: "images/Plan.svg",
                color: "#2196f3",
                direction: "left",
            };

            for (var i = 0; i < leaves.length; i++) {
                switch (leaves[i].name) {
                    case 'privilege':
                        privilege.count = leaves[i].count;
                        items.splice(0, 0, privilege);
                        break;
                    case 'paternity':
                        paternity.count = leaves[i].count;
                        items.splice(3, 0, paternity);
                        break;
                    case 'maternity':
                        maternity.count = leaves[i].count;
                        items.splice(3, 0, maternity);
                        break;
                    case 'Comp-off':
                        COMPOFF.count = leaves[i].count;
                        items.splice(1, 0, COMPOFF);
                        break;
                    default:
                        console.log("no item");
                        break;

                }

            };

            items.splice(2, 0, LOP);
            items.splice(4, 0, plan);
            return items;
        }


    });
