'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:SubheaderCtrl
 * @description
 * # SubheaderCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('SubheaderCtrl', function($scope, $rootScope, $location, $route, $window) {
        if ($location.path() === '/approvals') {
            $rootScope.view = "Approvals";
            $scope.screen = "Approvals";

        } else if ($location.path() === '/my_team') {
            $rootScope.view = "Approvals";
            $scope.screen = "MyTeam";
        } else if ($location.path() === '/admin_space') {
            $rootScope.view = "Approvals";
            $scope.screen = "AdminSpace";
        } else {
            $rootScope.view = "MySpace";
            $scope.screen = "MySpace";
        }

        $scope.changeScreen = function(scr) {

            if (scr == "Approvals") {
                $rootScope.view = "Approvals";
                $location.path('/approvals');
            } else if (scr == "MySpace") {
                $rootScope.view = "MySpace";
                $location.path('/leave_history');
            } else {

                $rootScope.view = "Approvals";
                $scope.screen = "MyTeam";
                $location.path('/my_team');
            }
        }
        $scope.refresh = function() {
            window.location.href = "#/leave_history";
            $window.location.reload(true);
        }
    });
