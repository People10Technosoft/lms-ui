'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.colorSelector
 * @description
 * # colorSelector
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
  .service('colorSelector', function () {
  	var colour={
  		A:"#B71C1C",
  		B:"#880E4F"
  	}
    this.selectColor=function(){
    	return colour;
    }
  });
