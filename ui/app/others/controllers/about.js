'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('AboutCtrl', function() {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
    });