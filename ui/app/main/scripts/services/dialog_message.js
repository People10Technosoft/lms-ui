'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.dialogMessage
 * @description
 * # dialogMessage
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
    .service('dialogMessage', function($mdDialog, $mdToast) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        this.showAlert = function(message) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(false)
                .title('Message')
                .textContent(message)
                .ariaLabel('Alert Dialog Demo')
                .ok('OK')
            );
        };

        // toast message with undo
        this.showActionToast = function() {
            var toast = $mdToast.simple()
                .textContent('Action Toast!')
                .action('UNDO')
                .highlightAction(false)
                .position('bottom left')
                .hideDelay(5);

            $mdToast.show(toast).then(function(response) {
                if (response == 'ok') {
                    return (response);
                }
            });
        };

        // default toast message
        this.showErrorToast = function(message) {
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .position("bottom left")
                .hideDelay(3000)
            );
        };

        // sample confirm
        this.loadOnConfirm = function() {
            sweet.show({
                title: 'Ajax request example',
                text: 'Submit to run ajax request',
                type: 'warning',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function(inputValue) {
                $timeout(function() {
                    sweet.show('Ajax request finished!');
                }, 2000);
            });
        };
    });
