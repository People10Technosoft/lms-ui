'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:ApplyCtrl
 * @description
 * # ApplyCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('ApplyCtrl', function($scope, $rootScope, $mdDialog, $localStorage, $mdMedia, $http, $timeout, leaveType, commonData) {

        var self = this;
        self.hidden = false;
        self.isOpen = false;
        self.hover = true;
        var leaveTypes = [];

        // Observe Screen change-MySpace, Approvals, MyTeam, Administration etc..
        commonData.observeScreen().then(null, null, function(data) {
            $scope.screen = data;

        });

        // Watching fab button
        $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
        $scope.$watch('demo.isOpen', function(isOpen) {
            if (isOpen) {
                $timeout(function() {
                    $scope.tooltipVisible = self.isOpen;
                }, 100);
            } else {
                $scope.tooltipVisible = self.isOpen;
            }
        });

        // Observe leave types
        commonData.observeLeaves().then(null, null, function(data) {
            self.items = data;
        });

        // API for get leave type for each employee
        $http.get($rootScope.nodeUrl + "specialLeaves?id=" + $rootScope.globals.currentUser.empId)
            .then(function(response) {
                leaveTypes = response.data;
                commonData.setLeaves(leaveType.leaveItems(leaveTypes));
                self.items = commonData.getLeaves();
            });

        // API for leave summary
        $http.get($rootScope.nodeUrl + "api/leave_requests/leaveStatistics?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
            .then(function(response) {
                $scope.leaveType = response.data;
            });

        // Open dialogue for new leave request
        $scope.showAdvanced = function(message) {

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;

            $mdDialog.show({
                controller: 'ApplyLeaveCtrl',
                templateUrl: 'employee/views/new_request.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                hasBackdrop: false,
                locals: {
                    message: message
                }

            })
        }
    });
