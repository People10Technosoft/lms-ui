'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:UpdateCtrl
 * @description
 * # UpdateCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('UpdateCtrl', function($scope, $rootScope, $http, $filter, commonData, sort, dialogMessage, leaveType, sweet, $mdToast, webService) {
        var paternity_count;
        var maternity_count;
        $scope.leaves = commonData.getLeaves();
        angular.forEach($scope.leaves, function(obj) {
            if (obj.name === "Paternity") {
                paternity_count = obj.count;

            } else if (obj.name === "Maternity") {
                maternity_count = obj.count;
            }
        });
        $scope.updateValue = true;
        if ($scope.updateDetails.numberOfdays == 0.5) {
            $scope.halfDay = true;
        }
        $scope.selectToDate = function() {
            var fromDate = $filter('date')($scope.updateDetails.fromDate, 'yyyy-MM-dd');
            var days = $scope.updateDetails.numberOfdays;
            if ($scope.updateDetails.typeOfLeave === "Paternity" || $scope.updateDetails.typeOfLeave === "Maternity") {
                $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&numberOfDays=" + days)
                    .then(function(response) {
                        $scope.updateDetails.toDate = new Date(response.data.toDate);
                    });
            } else {

                $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&numberOfDays=" + days)
                    .then(function(response) {
                        $scope.updateDetails.toDate = new Date(response.data.toDate);
                    });
            }

        }
        $scope.to_datePicker = function() {
            var fromDate = $filter('date')($scope.updateDetails.fromDate, 'yyyy-MM-dd');
            var days = $scope.updateDetails.numberOfdays;
            if (days !== 0.5) {
                var parsed = parseFloat(days, 10);
                if (parsed !== parsed) {
                    return null;
                } // check for NaN
                $scope.updateDetails.numberOfdays = Math.round(parsed);
                days = $scope.updateDetails.numberOfdays;
            }
            angular.forEach($scope.leaves, function(obj) {
                if ((obj.name === $scope.updateDetails.typeOfLeave) && (obj.count < days)) {
                    if (obj.name !== "Loss of Pay") {
                        $scope.updateDetails.numberOfdays = obj.count;
                    }
                }
            });
            if ($scope.updateDetails.typeOfLeave === "Paternity" || $scope.updateDetails.typeOfLeave === "Maternity") {
                $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&numberOfDays=" + days)
                    .then(function(response) {
                        $scope.updateDetails.toDate = new Date(response.data.toDate);
                    });
            } else {
                $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&numberOfDays=" + days)
                    .then(function(response) {
                        $scope.updateDetails.toDate = new Date(response.data.toDate);
                        if (response.data.halfDay === 1) {

                            $scope.halfDay = true;
                        }
                    });
            }

        }
        $scope.dayPicker = function() {
            var fromDate = $filter('date')($scope.updateDetails.fromDate, 'yyyy-MM-dd');
            var toDate = $filter('date')($scope.updateDetails.toDate, 'yyyy-MM-dd');
            if ($scope.updateDetails.typeOfLeave === "Paternity" || $scope.updateDetails.typeOfLeave === "Maternity") {
                $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&toDate=" + toDate)
                    .then(function(response) {
                        $scope.updateDetails.numberOfdays = Number(response.data.numberOfDays);
                        if (($scope.updateDetails.typeOfLeave === "Paternity") && ($scope.updateDetails.numberOfdays > paternity_count)) {
                            $scope.updateDetails.numberOfdays = paternity_count;
                        } else if (($scope.updateDetails.typeOfLeave === "Maternity") && ($scope.updateDetails.numberOfdays > maternity_count)) {
                            $scope.updateDetails.numberOfdays = maternity_count;
                        }
                        $http.get($rootScope.nodeUrl + "specialLeaveDateCalculation?fromDate=" + fromDate + "&numberOfDays=" + $scope.updateDetails.numberOfdays)
                            .then(function(response) {
                                $scope.updateDetails.toDate = new Date(response.data.toDate);
                            });
                    });
            } else {
                $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&toDate=" + toDate)
                    .then(function(response) {
                        $scope.updateDetails.numberOfdays = Number(response.data.numberOfDays);
                        angular.forEach($scope.leaves, function(obj) {
                            if ((obj.name === $scope.updateDetails.typeOfLeave) && (obj.count < $scope.updateDetails.numberOfdays)) {
                                if (obj.name !== "Loss of Pay") {
                                    $scope.updateDetails.numberOfdays = obj.count;
                                    $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&numberOfDays=" + $scope.updateDetails.numberOfdays)
                                        .then(function(response) {
                                            $scope.updateDetails.toDate = new Date(response.data.toDate);
                                        });
                                }
                            }
                        });
                    });
            }

        }
        $scope.halfDayCalc = function() {
            $scope.updateDetails.toDate = $scope.updateDetails.fromDate;
            if ($scope.halfDay) {
                $scope.updateDetails.numberOfdays = 0.5;
                $scope.updateDetails.halfDaySlot = 1;
            } else {
                $scope.updateDetails.numberOfdays = 1;
            }
        }
        $scope.leaveUpdate = function(validation, userDetail) {
            if (validation) {
                $scope.updateValue = false
                var result;
                var toast = $mdToast.simple()
                    .textContent('Updating...')
                    .action('UNDO')
                    .highlightAction(false)
                    .position('bottom left')
                    .hideDelay(3000);


                $mdToast.show(toast).then(function(response) {
                    if (response == 'ok') {
                        $scope.updateValue = true;

                    } else {
                        var data = {
                            fromDate: userDetail.fromDate,
                            toDate: userDetail.toDate,
                            numberOfDays: userDetail.numberOfdays,
                            type: userDetail.typeOfLeave,
                            comments: userDetail.comment,
                            employeeId: "" + $rootScope.globals.currentUser.empId,
                            halfDaySlot: userDetail.halfDaySlot,
                            approverId: $rootScope.employeeApprover,
                            action: "update",
                            id: userDetail.leaveRequestId
                        };


                        $scope.json = angular.toJson(data);
                        $http({
                                url: $rootScope.nodeUrl + 'api/leave_requests/' + userDetail.leaveRequestId + '?access_token=' + $rootScope.globals.currentUser.token,
                                method: "PUT",
                                data: $scope.json
                            })
                            .then(function(response) {
                                    $scope.updateValue = true;
                                    dialogMessage.showErrorToast("Your leave has been updated successfully");
                                    commonData.setUpdate(false);
                                    $http.get($rootScope.nodeUrl + "api/leave_requests/leaveStatistics?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                        .then(function(response) {
                                            $rootScope.myLeave = response.data;
                                        });

                                    $http.get($rootScope.nodeUrl + "api/leave_requests/getList?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                        .then(function(response) {
                                            commonData.setHistory(response.data);

                                        });
                                    $http.get($rootScope.nodeUrl + "api/leave_workflows/getLeaveWorkflows?leaveId=" + userDetail.leaveRequestId + "&employeeId=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                        .then(function(response) {
                                            commonData.setWorkflow(response.data);
                                        });
                                    $http.get($rootScope.nodeUrl + "specialLeaves?id=" + $rootScope.globals.currentUser.empId)
                                        .then(function(response) {
                                            commonData.setLeaves(leaveType.leaveItems(response.data));
                                        });
                                    var API_leaveSummary = $rootScope.nodeUrl + "api/leave_requests/leaveHistorySummary?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token;
                                    webService.getSummary(API_leaveSummary).success(function(data) {
                                        commonData.setEmployeeLeaveSummary(data);
                                    });
                                },
                                function(response) { // optional
                                    // failed
                                    dialogMessage.showErrorToast(response.data.error.message);
                                    $scope.updateValue = true;
                                });

                    }
                });

            }
        }
    });
