'use strict';

describe('Service: leaveType', function () {

  // load the service's module
  beforeEach(module('yoErpLmsApp'));

  // instantiate service
  var leaveType;
  beforeEach(inject(function (_leaveType_) {
    leaveType = _leaveType_;
  }));

  it('should do something', function () {
    expect(!!leaveType).toBe(true);
  });

});
