'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.colorSelector
 * @description
 * # colorSelector
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
  .service('getEmployeeDetails',['$http','$q', '$rootScope',function ($http, $q, $rootScope) {
  	var deferred = $q.defer();

    /*$http({
        url: 'http://localhost:5000/getUserDetails',
        method:'GET',
        withCredentials: true
    }).success(function (data) {
        deferred.resolve(data);
    }).error(function (msg) {
        console.log("Failure"+msg);
        $location.path('/login');
        deferred.reject(msg);
    });*/

    setTimeout(function () {
        deferred.resolve({success: true});
    }, 3000);

    return deferred.promise;
  }]);
