'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:PasswordResetCtrl
 * @description
 * # PasswordResetCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('PasswordResetCtrl', ['$scope', '$rootScope', '$location', '$http', 'sweet', '$route', 'dialogMessage', function($scope, $rootScope, $location, $http, sweet, $route, dialogMessage) {

        $scope.resetPassword = function(pass1, pass2) {

            if (pass1 === pass2) {
                var empId;
                var token;
                $scope.errorMessage = "";
                var parameters = $route.current.params;
                if (parameters.id) {

                    empId = parameters.id;
                    token = parameters.access_token;
                } else {
                    empId = $rootScope.globals.currentUser.empId;
                    token = $rootScope.globals.currentUser.token;
                }

                var data = {
                    action: "resetPasswordRequest",
                    password: pass1,
                    id: empId
                }
                $scope.json = angular.toJson(data);
                $http({
                        url: $rootScope.nodeUrl + 'api/employees/' + empId + '?access_token=' + token,
                        method: "PUT",
                        data: $scope.json
                    })
                    .then(function(response) {
                            $http({
                                    url: $rootScope.nodeUrl + "api/employees/logout?access_token=" + token,
                                    method: "POST"
                                })
                                .then(function(response) {
                                        // success

                                        dialogMessage.showErrorToast('Your password has been updated successfully');
                                        $rootScope.globals = {};
                                        window.localStorage.clear();
                                        window.location.href = '#/login';

                                    },
                                    function(response) { // optional
                                        // failed
                                        dialogMessage.showErrorToast(response.data.error.message);
                                    });

                        },
                        function(response) { // optional
                            // failed
                            dialogMessage.showErrorToast(response.data.error.message);
                        });

            } else {
                $scope.errorMessage = "New Password and confirm Password should be same";
            }
        }
    }]);
