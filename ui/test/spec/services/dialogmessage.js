'use strict';

describe('Service: dialogMessage', function () {

  // load the service's module
  beforeEach(module('yoErpLmsApp'));

  // instantiate service
  var dialogMessage;
  beforeEach(inject(function (_dialogMessage_) {
    dialogMessage = _dialogMessage_;
  }));

  it('should do something', function () {
    expect(!!dialogMessage).toBe(true);
  });

});
