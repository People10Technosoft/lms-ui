'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:AdminSpaceCtrl
 * @description
 * # AdminSpaceCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('AdminSpaceCtrl', ['$scope', '$mdDialog', 'commonData', '$rootScope', 'webService', 'dialogMessage', '$http',
        function($scope, $mdDialog, commonData, $rootScope, webService, dialogMessage, $http) {

            $scope.query = {}
            $scope.queryBy = '$'

            commonData.setScreen("AdminSpace");
            $scope.user;
            $scope.selectedUserIndex = undefined;
            $scope.title1 = undefined;
            $scope.editBasic = false;
            $scope.editPersonal = false;
            $scope.editContactAddress = false;
            $scope.editPermanentAddress = false;
            $scope.editContactNumber = false;
            $scope.infoColor = commonData.getInfoColors();
            $scope.form = undefined;
            $scope.progress = true;
            $scope.progress_mode = "indeterminate";
            $scope.addNew = false;
            $scope.visibleScreen = "MyTeam";
            $scope.buttonTitle = "PROFILE INFO";
            $scope.tabContent = 'main/views/about.html';
            var formNo;
            $scope.submitTitle = "Continue";
            $scope.history = [];
            $scope.leave;
            $scope.workflow = [];

            $scope.tooltip = commonData.getTooltip();

            $scope.colors = commonData.getLeaveColor();

            commonData.observeWorkflow().then(null, null, function(data) {
                $scope.workflow = data;
            });

            //Observe leave eligibility count
            commonData.observeEligibility().then(null, null, function(elg) {
                $scope.team.eligibilityCount = elg;
                if ($scope.team.eligibilityCount.length >= 2) {
                    $scope.diasableAdd = true;
                }
            });

            //Observe Employee List
            commonData.observeEmployeeList().then(null, null, function(empList) {
                $scope.empData = empList;
            });

            //Observe Profile Information of an empoyee
            commonData.observeProfileInfo().then(null, null, function(data) {
                $scope.user = data;
            });

            //Observe tab content change
            commonData.observeTabContent().then(null, null, function(url) {
                $scope.tabContent = url;
            });

            //Observe Leave History of an employee
            commonData.observeHistory().then(null, null, function(data) {
                $scope.history = data;
            });

            //Observe Leave Summary of an employee
            commonData.observeLeaveSummary().then(null, null, function(data) {
                $scope.leave = data;
            });

            //API for get all employee list
            var getEmployeeAPI = $rootScope.nodeUrl + "api/employees/getEmployeeList?access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(getEmployeeAPI).success(function(data) {
                commonData.setEmployeeList(data);
                $scope.progress = false;
                $scope.progress_mode = "null";
            });

            //API for get key Tables
            var designationAPI = $rootScope.nodeUrl + "api/erp_keys/designationList?access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(designationAPI).success(function(data) {
                $scope.designation = data;
            });

            var bloodGroupAPI = $rootScope.nodeUrl + "api/erp_keys/bloodGroupList?access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(bloodGroupAPI).success(function(data) {
                $scope.bldGrp = data;
            });

            var departmentAPI = $rootScope.nodeUrl + "api/erp_keys/departmentList?access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(departmentAPI).success(function(data) {
                $scope.department = data;
            });

            var managerAPI = $rootScope.nodeUrl + "api/employees/getManagerList?access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(managerAPI).success(function(data) {
                $scope.manager = data;
            });

            //Observe form changes in the dialog box
            commonData.observeDialogData().then(null, null, function(dialogColor) {
                $scope.formTitle = dialogColor.title;
                $scope.formcolor = dialogColor.color;
                $scope.btncolor = dialogColor.btnColor;
            });

            $scope.tabFunction = function(title, url) {
                $scope.buttonTitle = title;
                commonData.setTabContent(url);
            }

            //Collapse the card view
            $scope.selectUserIndex1 = function(index, title) {
                if ($scope.selectedUserIndex1 !== index || $scope.title1 !== title) {
                    $scope.selectedUserIndex1 = index;
                    $scope.title1 = title;
                } else {
                    $scope.selectedUserIndex1 = undefined;
                    $scope.title1 = undefined;
                }
            };

            //Expand card view
            $scope.memberDetails = function(event, index, id, title) {
                event.stopPropagation();
                $rootScope.teamEmpId = id;
                var profileInfoAPI = $rootScope.nodeUrl + "api/employees/profileInformation?id=" + id + "&access_token=" + $rootScope.globals.currentUser.token;
                webService.getSummary(profileInfoAPI).success(function(details) {
                    commonData.setProfileInfo(details);
                    var leaveSummaryAPI = $rootScope.nodeUrl + "api/leave_requests/leaveStatistics?id=" + id + "&access_token=" + $rootScope.globals.currentUser.token;
                    webService.getSummary(leaveSummaryAPI).success(function(data) {
                        commonData.setLeaveSummary(data);
                    });
                    var leaveHistoryAPI = $rootScope.nodeUrl + "api/leave_requests/getList?id=" + id + "&access_token=" + $rootScope.globals.currentUser.token;
                    webService.getSummary(leaveHistoryAPI).success(function(data) {
                        commonData.setHistory(data);
                    });
                });
                $scope.selectedUserIndex1 = index;
                $scope.title1 = title;
            };

            /*Open model for adding new employee*/

            $scope.editData = function(ev, title, bgcolor1, bgcolor2, url, data) {
                ev.stopPropagation();
                commonData.setDialogData(title, bgcolor1, bgcolor2);
                data.dob = new Date(data.dob);
                data.joiningDate = new Date(data.joiningDate);
                data.confirmationDate = new Date(data.confirmationDate);
                $scope.team = data;
                $scope.form = url;

                $mdDialog.show({
                    templateUrl: 'main/views/user_detail_edit_form.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    scope: $scope.$new()
                });
            }

            /*Close model for adding new employee*/

            $scope.closeDialog = function(id) {
                var profileInfoAPI = $rootScope.nodeUrl + "api/employees/profileInformation?id=" + $scope.team.id + "&access_token=" + $rootScope.globals.currentUser.token;
                webService.getSummary(profileInfoAPI).success(function(details) {
                    commonData.setProfileInfo(details);
                    /*$scope.user = details;*/
                });
                $scope.addNew = false;
                $scope.diasableAdd = false;
                $mdDialog.hide();
            };

            /*Change the url to other edit details*/

            $scope.editDataForm = function(ev, url, title, bgcolor1, bgcolor2) {
                ev.stopPropagation();
                commonData.setDialogData(title, bgcolor1, bgcolor2);
                $scope.form = url;

            };

            /*function for update employee status(active/inactive)*/

            $scope.changeStatus = function(id, status) {
                var url3 = $rootScope.nodeUrl + "api/employees/setActiveFieldOfEmployee?access_token=" + $rootScope.globals.currentUser.token;
                var data = {
                    "employeeId": id,
                    "status": status
                };
                webService.PostService(url3, data).success(function(response) {
                    $scope.selectedUserIndex1 = undefined;
                    $scope.title1 = undefined;
                    var profileInfoAPI = $rootScope.nodeUrl + "api/employees/profileInformation?id=" + id + "&access_token=" + $rootScope.globals.currentUser.token;
                    webService.getSummary(getEmployeeAPI).success(function(data) {
                        commonData.setEmployeeList(data);
                    });
                    webService.getSummary(profileInfoAPI).success(function(details) {
                        commonData.setProfileInfo(details)
                    });

                });
            }

            /*function for update employee details*/

            $scope.editEmployee = function(data, title) {
                $scope.team.action = "editRequest";
                if (title === "Leave Eligibility") {
                    var elgData = [];
                    var elg;
                    angular.forEach($scope.team.eligibilityCount, function(obj) {
                        elg = {
                            eligibilityYear: obj.year,
                            typeOfLeaves: 'Privilege',
                            eligibilityCount: obj.privilege
                        };
                        elgData.push(elg);
                        if ($scope.team.gender == 'F') {
                            elg = {
                                eligibilityYear: obj.year,
                                typeOfLeaves: 'Maternity',
                                eligibilityCount: obj.maternity
                            }
                        } else {
                            elg = {
                                eligibilityYear: obj.year,
                                typeOfLeaves: 'Paternity',
                                eligibilityCount: obj.paternity
                            }
                        }
                        elgData.push(elg);

                    });
                    var eligibilityData = {
                        employeeId: $scope.team.id,
                        leaveEligibility: elgData
                    };
                    //API for update employee leave eligibility details
                    var leaveEligibilityAPI = $rootScope.nodeUrl + "api/employees/updateLeaveEligibility?access_token=" + $rootScope.globals.currentUser.token;
                    webService.PostService(leaveEligibilityAPI, eligibilityData).success(function(response) {
                        console.log("Updated successfully");
                    });

                };
                //API for update employee details
                var url_updateEmployee = $rootScope.nodeUrl + "api/employees/" + $scope.team.id + "?access_token=" + $rootScope.globals.currentUser.token;
                $http({
                        url: url_updateEmployee,
                        method: "PUT",
                        data: $scope.team
                    })
                    .then(function(response) {
                            $scope.team.action = undefined;
                            $mdDialog.hide();
                            $scope.selectedUserIndex = undefined;
                            dialogMessage.showErrorToast('Updated successfully');
                            $scope.diasableAdd = false;
                            webService.getSummary(getEmployeeAPI).success(function(data) {
                                commonData.setEmployeeList(data);

                            });
                            var profileInfoAPI = $rootScope.nodeUrl + "api/employees/profileInformation?id=" + $scope.team.id + "&access_token=" + $rootScope.globals.currentUser.token;
                            webService.getSummary(profileInfoAPI).success(function(details) {
                                commonData.setProfileInfo(details);
                            });
                        },
                        function(response) {
                            $scope.diasableAdd = false;
                            dialogMessage.showErrorToast(response.data.error.message);
                            if (response.data.error.message === 'Manager Name Is Mandatory') {
                                commonData.setDialogData('Professional Details', '#8cc474', '#aed69e');
                                $scope.form = 'main/views/basicinfo_edit_form.html';
                            } else {
                                commonData.setDialogData('Personal Details', '#4dbfd9', '#82d2e4');
                                $scope.form = 'main/views/personalinfo_edit_form.html';
                            }

                        });
            }


            /*Leave Eligibility*/

            $scope.addEligibility = function() {
                if ($scope.team.eligibilityCount.length < 2) {
                    $scope.diasableAdd = false;
                    var lastLeaveData = $scope.team.eligibilityCount[$scope.team.eligibilityCount.length - 1];
                    var year = Number(lastLeaveData.year) + 1;
                    var item = {
                        year: year.toString(),
                        privilege: 0,
                        maternity: 0,
                        paternity: 0
                    }
                    $scope.team.eligibilityCount.push(item);
                    commonData.setEligibility($scope.team.eligibilityCount);
                } else {
                    $scope.diasableAdd = true;
                }

            }






            /*ADD EMPLOYEE*/



            // $scope.dummy = ["Engineering Lead", "O-", "1", "DDDDDD"];
            /*
                $scope.empData = [{
                "name": "sangeetha",
                "empId": "P10E0154",
                "emailId": "erp4people10@gmail.com",
                "phoneNo": "9986986503",
                "dob": "1985-08-16T00:00:00.000Z",
                "department":"Delivery",
                "bloodGroup":"O-",
                "panNumber":"ADDD122229",
                "empType":"Confirmed",
                "joiningDate": "2016-05-16T00:00:00.000Z",
                "levelOfApprovalRequired": 1,
                "emergencyContactPhoneNo": "9876543210",
                "gender": "F",
                "status":"Active",
                "permanentAddressDetails": {
                    "permanentAddressLine1": "58 B, Model Town",
                    "permanentAddressLine2": "Phagwara",
                    "city": "Phagwara",
                    "state": "Punjab",
                    "country": "India",
                    "pincode": "144401"
                },
                "localAddressDetails": {
                    "localAddressLine1": "B 313, Mahaveer Seasons, 24th Main Road",
                    "localAddressLine2": "Sector 2, HSR Layout",
                    "city": "Bangalore",
                    "state": "Karnataka",
                    "country": "India",
                    "pincode": "560102"
                },
                "designation": "Engineering Lead",
                "managerName": "adarsh"
            }, {
                "name": "sangeetha",
                "empId": "P10E0154",
                "emailId": "erp4people10@gmail.com",
                "phoneNo": "9986986503",
                "department":"Delivery",
                "bloodGroup":"O-",
                "panNumber":"ADDD122229",
                "empType":"Confirmed",
                "department":"Delivery",
                "joiningDate": "2016-05-16T00:00:00.000Z",
                "levelOfApprovalRequired": 1,
                "emergencyContactPhoneNo": "9876543210",
                "gender": "F",
                "status":"Active",
                "permanentAddressDetails": {
                    "permanentAddressLine1": "58 B, Model Town, Phagwara",
                    "permanentAddressLine2": "Phagwara",
                    "city": "Phagwara",
                    "state": "Punjab",
                    "country": "India",
                    "pincode": "144401"
                },
                "localAddressDetails": {
                    "localAddressLine1": "B 313, Mahaveer Seasons, 24th Main Road",
                    "localAddressLine2": "Sector 2, HSR Layout",
                    "city": "Bangalore",
                    "state": "Karnataka",
                    "country": "India",
                    "pincode": "560102"
                },
                "designation": "Engineering Lead",
                "managerName": "adarsh"
            }, {
                "name": "sangeetha",
                "empId": "P10E0154",
                "emailId": "erp4people10@gmail.com",
                "phoneNo": "9986986503",
                "department":"Delivery",
                "bloodGroup":"O-",
                "panNumber":"ADDD122229",
                "empType":"Confirmed",
                "department":"Delivery",
                "joiningDate": "2016-05-16T00:00:00.000Z",
                "levelOfApprovalRequired": 1,
                "emergencyContactPhoneNo": "9876543210",
                "gender": "F",
                "status":"Active",
                "permanentAddressDetails": {
                    "permanentAddressLine1": "58 B, Model Town, Phagwara",
                    "permanentAddressLine2": "Phagwara",
                    "city": "Phagwara",
                    "state": "Punjab",
                    "country": "India",
                    "pincode": "144401"
                },
                "localAddressDetails": {
                    "localAddressLine1": "B 313, Mahaveer Seasons, 24th Main Road",
                    "localAddressLine2": "Sector 2, HSR Layout",
                    "city": "Bangalore",
                    "state": "Karnataka",
                    "country": "India",
                    "pincode": "560102"
                },
                "designation": "Engineering Lead",
                "managerName": "adarsh"
            }];*/


            /*var jsonData={ firstName: 'asd',
              lastName: 'asd',
              gender: 'M',
              dob: '2016-08-07T18:30:00.000Z',
              bloodGroupId: '2',
              nationality: 'asda',
              localAddressDetails:
               { localAddressLine1: 'asdas',
                 localAddressLine2: 'asdasd',
                 city: 'asdasd',
                 state: 'asdas',
                 country: 'asdsad',
                 pincode: '123456' },
              permanentAddressDetails:
               { permanentAddressLine1: 'asdasd',
                 permanentAddressLine2: 'asdasd',
                 city: 'asdasd',
                 state: 'asdasd',
                 country: 'dasdasd',
                 pincode: '123456' },
              designationId: '2',
              joiningDate: '2016-08-07T18:30:00.000Z',
              departmentId: '2',
              managerId: '1033',
              confirmationDate: '2016-08-25T18:30:00.000Z',
              levelOfApprovalRequired: 0,
              empId: 'P10E0290',
              email: 'asd@gmail.com',
              phoneNo: '7894561236',
              emergencyContactName: 'asdas',
              emergencyContactPhoneNo: '1234567893',
              name: 'asdasd',
              permanentAddressLine1: 'asdasd',
              permanentAddressLine2: 'asdasd',
              permanentAddressCity: 'asdasd',
              permanentAddressState: 'asdasd',
              permanentAddressCountry: 'dasdasd',
              permanentAddressPincode: '123456',
              localAddressLine1: 'asdas',
              localAddressLine2: 'asdasd',
              localAddressCity: 'asdasd',
              localAddressState: 'asdas',
              localAddressCountry: 'asdsad',
              localAddressPincode: '123456',
              designation: '2',
              reportingManagerId: '1033',
              bloodGroup: '2',
              activeInactiveDate: '2016-08-07T18:30:00.000Z',
              password: '$2a$10$udE0ZMJqUbq1t6IM/tE/K.I8tZWx2FaCHF1Ql8pohcHBBUXqREz3m' }
              var jsonData= angular.toJson(jsonData);
              console.log(jsonData);*/
        }
    ]);
