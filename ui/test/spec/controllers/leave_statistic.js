'use strict';

describe('Controller: LeaveStatisticCtrl', function () {

  // load the controller's module
  beforeEach(module('yoErpLmsApp'));

  var LeaveStatisticCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LeaveStatisticCtrl = $controller('LeaveStatisticCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LeaveStatisticCtrl.awesomeThings.length).toBe(3);
  });
});
