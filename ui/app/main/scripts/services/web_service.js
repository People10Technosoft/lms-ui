'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.webService
 * @description
 * # webService
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
    .service('webService', function($http, commonData) {

        // http get method
        this.getSummary = function(url) {
            return $http.get(url);
        }

        // http post method
        this.PostService = function(url, data) {
            return $http.post(url, data);
        }

        // http put method
        this.PutService = function(url, data) {
            return $http.put(url, data);
        }

    });
