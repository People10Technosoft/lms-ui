'use strict';

/**
 * @ngdoc directive
 * @name yoErpLmsApp.directive:leaveSummary
 * @description
 * # leaveSummary
 */
angular.module('yoErpLmsApp')
    .directive('leaveSummary', function() {
        return {
            controller: 'MyTeamCtrl',
            templateUrl: 'manager/views/team_leave_summary.html',
            scope: {
                leave: "=name"
            },
            restrict: 'EA'
        };
    });
