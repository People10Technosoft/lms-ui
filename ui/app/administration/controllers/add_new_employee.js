'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:AddNewEmployeeCtrl
 * @description
 * # AddNewEmployeeCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('AddNewEmployeeCtrl', ['$scope', '$mdDialog', 'commonData', '$rootScope', 'webService', 'dialogMessage', '$http', function($scope, $mdDialog, commonData, $rootScope, webService, dialogMessage, $http) {

        $scope.team = {};
        var formNo;
        $scope.infoColor = commonData.getInfoColors();
        $scope.form = undefined;
        $scope.submitTitle = "Save";
        $scope.team.levelOfApprovalRequired = 0;
        $scope.diasableAdd = false;
        var d = new Date();
        var n = d.getFullYear().toString();
        $scope.team.eligibilityCount = [{
            year: n,
            privilege: 0,
            maternity: 0,
            paternity: 0
        }];

        commonData.observeScreen().then(null, null, function(data) {
            $scope.screen = data;
        });


        /*Ordered form url*/

        var formUrlInfo = [

            {
                url: 'main/views/personalinfo_edit_form.html',
                title: 'Personal Details',
                bgcolor1: '#4dbfd9',
                bgcolor2: '#82d2e4'
            }, {
                url: 'main/views/basicinfo_edit_form.html',
                title: 'Professional Details',
                bgcolor1: '#8cc474',
                bgcolor2: '#aed69e'
            }, {
                url: 'main/views/local_add_info_edit_form.html',
                title: 'Contact Details',
                bgcolor1: '#bc5679',
                bgcolor2: '#d089a1'
            }, {
                url: 'main/views/leave_eligibility_edit_form.html',
                title: 'Leave Eligibility',
                bgcolor1: '#76a7fa',
                bgcolor2: '#9fc1fb'
            }
        ];

        // Restrict to 2 year eligibility
        commonData.observeAddEligibility().then(null, null, function(elg) {
            $scope.team.eligibilityCount = elg;
            if ($scope.team.eligibilityCount.length >= 2) {
                $scope.diasableAdd = true;
            }
        });

        /*add default email*/
        $scope.addEmail = function() {
            $scope.team.email = $scope.team.firstName.replace(/ /g, '');;

            if ($scope.team.lastName != null) {
                $scope.team.email = $scope.team.email + "." + $scope.team.lastName.replace(/ /g, '');
            }
            $scope.team.email = $scope.team.email.toLowerCase() + "@people10.com";
        }

        /*LeaveEligibility*/

        $scope.addEligibility = function() {
            var lastLeaveData = $scope.team.eligibilityCount[$scope.team.eligibilityCount.length - 1];
            var year = Number(lastLeaveData.year) + 1;
            var item = {
                year: year.toString(),
                privilege: 0,
                maternity: 0,
                paternity: 0
            }
            $scope.team.eligibilityCount.push(item);
            commonData.setAddEligibility($scope.team.eligibilityCount);

        }

        // API call for getting key table values
        var designationAPI = $rootScope.nodeUrl + "api/erp_keys/designationList?access_token=" + $rootScope.globals.currentUser.token;
        webService.getSummary(designationAPI).success(function(data) {
            $scope.designation = data;
        });

        var bloodGroupAPI = $rootScope.nodeUrl + "api/erp_keys/bloodGroupList?access_token=" + $rootScope.globals.currentUser.token;
        webService.getSummary(bloodGroupAPI).success(function(data) {
            $scope.bldGrp = data;
        });

        var departmentAPI = $rootScope.nodeUrl + "api/erp_keys/departmentList?access_token=" + $rootScope.globals.currentUser.token;
        webService.getSummary(departmentAPI).success(function(data) {
            $scope.department = data;
        });

        var managerAPI = $rootScope.nodeUrl + "api/employees/getManagerList?access_token=" + $rootScope.globals.currentUser.token;
        webService.getSummary(managerAPI).success(function(data) {
            $scope.manager = data;

        });

        commonData.observeDialogData().then(null, null, function(dialogColor) {
            $scope.formTitle = dialogColor.title;
            $scope.formcolor = dialogColor.color;
            $scope.btncolor = dialogColor.btnColor;
        });

        /*Open model for adding new employee*/

        $scope.addEmployee = function(ev, title, bgcolor1, bgcolor2, url) {
            ev.stopPropagation();
            commonData.setDialogData(title, bgcolor1, bgcolor2);
            $scope.form = url;
            $scope.addNew = true;
            $mdDialog.show({
                templateUrl: 'main/views/user_detail_edit_form.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                scope: $scope.$new()
            });
        }

        /*Close model for adding new employee*/

        $scope.closeDialog = function(id) {
            $scope.team = {};
            $scope.diasableAdd = false;
            $scope.team.levelOfApprovalRequired = 0;
            $scope.team.managerId = '1002';
            $scope.team.eligibilityCount = [{
                year: '2016',
                privilege: 0,
                maternity: 0,
                paternity: 0
            }];
            $mdDialog.hide();
            console.log("closing...");
        };

        /*Change the url to other edit details*/

        $scope.editDataForm = function(ev, url, title, bgcolor1, bgcolor2) {
            ev.stopPropagation();
            commonData.setDialogData(title, bgcolor1, bgcolor2);
            $scope.form = url;
        };

        /*function for adding new employee*/

        $scope.addEmployeeDetails = function(ev, currentForm, team) {

            var elgData = [];
            var elg;
            angular.forEach($scope.team.eligibilityCount, function(obj) {
                elg = {
                    eligibilityYear: obj.year,
                    typeOfLeaves: 'Privilege',
                    eligibilityCount: obj.privilege
                };
                elgData.push(elg);
                if ($scope.team.gender == 'F') {
                    elg = {
                        eligibilityYear: obj.year,
                        typeOfLeaves: 'Maternity',
                        eligibilityCount: obj.maternity
                    }
                } else {
                    elg = {
                        eligibilityYear: obj.year,
                        typeOfLeaves: 'Paternity',
                        eligibilityCount: obj.paternity
                    }
                }
                elgData.push(elg);

            });



            /*add other information*/


            team.leaveEligibility = elgData;
            console.log(team);

            var addEmployeeAPI = $rootScope.nodeUrl + "api/employees?access_token=" + $rootScope.globals.currentUser.token;
            $http({
                    url: addEmployeeAPI,
                    method: "POST",
                    data: team
                })
                .then(function(response) {
                        $mdDialog.cancel();
                        var getEmployeeAPI = $rootScope.nodeUrl + "api/employees/getEmployeeList?access_token=" + $rootScope.globals.currentUser.token;
                        webService.getSummary(getEmployeeAPI).success(function(data) {
                            commonData.setEmployeeList(data);
                        });
                        dialogMessage.showErrorToast("New Employee is added successfully");
                        $scope.team = {};
                        $scope.diasableAdd = false;
                        $scope.team.levelOfApprovalRequired = 0;
                        $scope.team.managerId = '1002';
                        $scope.team.eligibilityCount = [{
                            year: '2016',
                            privilege: 0,
                            maternity: 0,
                            paternity: 0
                        }];
                    },
                    function(response) {
                        dialogMessage.showErrorToast(response.data.error.message);
                        if (response.data.error.message === 'Manager Name Is Mandatory') {
                            commonData.setDialogData('Professional Details', '#8cc474', '#aed69e');
                            $scope.form = 'main/views/basicinfo_edit_form.html';
                        } else {
                            commonData.setDialogData('Personal Details', '#4dbfd9', '#82d2e4');
                            $scope.form = 'main/views/personalinfo_edit_form.html';
                        }
                    });

        }


    }]);
