'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('LoginCtrl', ['$scope', '$http', '$rootScope', '$location', '$localStorage', 'sweet', '$route', '$window', 'commonData', 'dialogMessage',
        function($scope, $http, $rootScope, $location, sweet, $localStorage, $route, $window, commonData, dialogMessage) {

            $scope.loginUser = function(user, valid) {
                if (valid) {

                    var data = {
                        email: $scope.user.email,
                        password: $scope.user.password
                    };
                    $scope.json = angular.toJson(data);
                    $http({
                            url: $rootScope.nodeUrl + 'login',
                            method: "POST",
                            data: $scope.json
                        })
                        .then(function(response) {
                                // First time login
                                if (response.data.freshLogin == 1) {
                                    $rootScope.globals = {
                                        currentUser: {
                                            token: response.data.token.id,
                                            empId: response.data.token.userId,
                                            freshLogin: response.data.freshLogin,
                                            name: response.data.name,
                                            fName: response.data.firstName,
                                            lName: response.data.lastName
                                        }
                                    };
                                    window.sessionStorage['globals'] = JSON.stringify($rootScope.globals);
                                    $location.path("/password_reset");
                                }
                                // Mangaer role
                                else if (response.data.manager) {
                                    $rootScope.globals = {
                                        currentUser: {
                                            token: response.data.token.id,
                                            empId: response.data.token.userId,
                                            manager: response.data.manager,
                                            loginValue: true,
                                            name: response.data.name,
                                            fName: response.data.firstName,
                                            lName: response.data.lastName,
                                            admin: response.data.admin
                                        }
                                    };
                                    window.sessionStorage['globals'] = JSON.stringify($rootScope.globals);
                                    window.location.href = "#/approvals";
                                }
                                // Admin role
                                else if (response.data.admin) {
                                    $rootScope.globals = {
                                        currentUser: {
                                            token: response.data.token.id,
                                            empId: response.data.token.userId,
                                            loginValue: true,
                                            name: response.data.name,
                                            fName: response.data.firstName,
                                            lName: response.data.lastName,
                                            admin: response.data.admin
                                        }
                                    };
                                    window.sessionStorage['globals'] = JSON.stringify($rootScope.globals);
                                    window.location.href = "#/admin_space";
                                }
                                // Employee
                                else {
                                    $rootScope.globals = {
                                        currentUser: {
                                            token: response.data.token.id,
                                            empId: response.data.token.userId,
                                            loginValue: true,
                                            name: response.data.name,
                                            fName: response.data.firstName,
                                            lName: response.data.lastName,
                                            admin: response.data.admin
                                        }
                                    };
                                    window.sessionStorage['globals'] = JSON.stringify($rootScope.globals);
                                    window.location.href = "#/leave_history";
                                    commonData.setScreen("MySpace");
                                }
                            },
                            function(response) { // optional
                                // failed
                                $scope.message = response.data;
                                $location.path("/login");
                            });
                }

            }
            $scope.forgotPass = function() {
                $location.path("/forgot_password");
            }
            $scope.signIn = function() {
                $location.path("https://10.0.1.73:5000/auth/google");
            }
        }
    ]);
