'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.leaveAction
 * @description
 * # leaveAction
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
    .service('leaveAction', function($http, $rootScope, dialogMessage, commonData, sweet, $mdDialog, $route) {
        this.actionTaken = function(data, action, comment) {
            var actions = [{
                "fromEmployee": $rootScope.globals.currentUser.empId,
                "comments": comment,
                "leaveRequestId": data.leaveId,
                "actionByManager": action,
                "employeeId": data.fromEmployee
            }];
            var approverData = angular.toJson(actions);
            $http({
                    url: $rootScope.nodeUrl + 'managerActions',
                    method: "POST",
                    data: approverData
                })
                .then(function(response) {
                        $mdDialog.hide();
                        $http.get($rootScope.nodeUrl + "api/leave_requests/leaveRequestsWaitingForApproval?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                            .then(function(response) {
                                commonData.setRequest(response.data);
                            });
                        dialogMessage.showErrorToast('Your ' + action + ' action completed successfully');
                    },
                    function(response) { // optional
                        dialogMessage.showErrorToast('Action failed');
                    });
        }
        this.actionToAll = function(data) {

            var actions = [];
            for (var i = 0; i < data.length; i++) {
                var action = {};
                action.fromEmployee = $rootScope.globals.currentUser.empId;
                action.comments = "";
                action.leaveRequestId = data[i].leaveId;
                action.actionByManager = "Approve";
                action.employeeId = data[i].fromEmployee;
                actions.push(action);
            }
            var approverData = angular.toJson(actions);
            $http({
                    url: $rootScope.nodeUrl + 'managerActions',
                    method: "POST",
                    data: approverData
                })
                .then(function(response) {
                        $http.get($rootScope.nodeUrl + "api/leave_requests/leaveRequestsWaitingForApproval?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                            .then(function(response) {
                                commonData.setRequest(response.data);
                            });
                        dialogMessage.showErrorToast('Leaves are approved successfully');
                    },
                    function(response) { // optional
                        dialogMessage.showErrorToast('Action failed');
                    });
        }

    });
