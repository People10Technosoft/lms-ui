'use strict';

describe('Controller: AddNewEmployeeCtrl', function () {

  // load the controller's module
  beforeEach(module('yoErpLmsApp'));

  var AddNewEmployeeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AddNewEmployeeCtrl = $controller('AddNewEmployeeCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AddNewEmployeeCtrl.awesomeThings.length).toBe(3);
  });
});
