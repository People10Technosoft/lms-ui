angular.module('yoErpLmsApp')
    .directive('leaveSummaryCard', function() {
        return {
            restrict: 'EA',
            templateUrl: 'main/scripts/directives/leave_summary/leave_summary.html',
            scope: {
                employeeid: "="
            },
            controller: function($scope, $rootScope, webService, commonData) {
                // console.log("leave Summary Directive: =" + $scope.content);
                commonData.observeEmployeeLeaveSummary().then(null, null, function(data) {
                    $scope.leaveSummary = data;
                });
                var API_leaveSummary = $rootScope.nodeUrl + "api/leave_requests/leaveHistorySummary?id=" + $scope.employeeid + "&access_token=" + $rootScope.globals.currentUser.token;
                webService.getSummary(API_leaveSummary).success(function(data) {
                    commonData.setEmployeeLeaveSummary(data);
                    // $scope.leaveSummary = data;
                });

            }
        };
    });
