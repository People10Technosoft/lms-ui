angular.module('yoErpLmsApp')
    .directive('detail', function() {
        return {
            restrict: 'EA',
            templateUrl: 'main/scripts/directives/section_card/detail.html',
            scope: {
                options: "=",
                content: "=",
                datefilter: "@"
            },
            controller: function($scope, $filter) {
                if ($scope.content.fromDate) {
                    $scope.filteredContent = $filter($scope.datefilter)($scope.content.fromDate) + " to " + $filter($scope.datefilter)($scope.content.toDate);
                } else {
                    $scope.filteredContent = $filter($scope.datefilter)($scope.content);
                }

            }
        };
    });
