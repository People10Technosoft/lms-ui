'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:LeaveUpdateCtrl
 * @description
 * # LeaveUpdateCtrl
 * Controller of the yoErpLmsApp
 */

// Not in Use
angular.module('yoErpLmsApp')
    .controller('LeaveUpdateCtrl', function($scope, $rootScope, $mdDialog, $http, $filter, message, commonData, sort, dialogMessage, webService) {

        $scope.leaves = [{
            name: "Privilege",
            icon: "images/Privilege.svg",
            direction: "left",
            color: " #e91e63"
        }, {
            name: "LOP",
            icon: "images/LOP.svg",
            direction: "left",
            color: "#9C27B0"
        }];

        $scope.user = message;
        $scope.user.employeeId = $rootScope.globals.currentUser.empId;

        $scope.user.fromDate = new Date($scope.user.fromDate);
        $scope.user.toDate = new Date($scope.user.toDate);
        $scope.user.noOfdays = Number($scope.user.noOfdays);
        if ($scope.user.noOfdays == 0.5) {
            $scope.halfDay = true;
        }
        $http.get($rootScope.nodeUrl + "api/employees/getApprovars?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
            .then(function(response) {
                $scope.managers = response.data;
                var index = $scope.user.workflow.length - 1;
                $scope.user.approverId = $scope.user.workflow[index].toEmployee;
                for (var i = 0; i < $scope.managers.length; i++) {
                    if ($scope.user.approverId == $scope.managers[i].id) {
                        $scope.approverId = $scope.managers[i].name.toUpperCase();
                        break;
                    }
                }


            });

        $scope.selectApprover = function(id) {
            $scope.user.approverId = id;
        }



        $scope.closeDialog = function() {
            // console.log("close btn")
            $mdDialog.cancel();
        };
        $scope.halfDayCalc = function() {
            $scope.user.toDate = $scope.user.fromDate;
            if ($scope.halfDay) {
                $scope.user.noOfdays = 0.5;
                $scope.user.halfDaySlot = 1;
            } else {
                $scope.user.noOfdays = 1;


            }
        }
        $scope.selectToDate = function() {
            $scope.user.toDate = $scope.user.fromDate;
            var fromDate = $filter('date')($scope.user.fromDate, 'yyyy-MM-dd');
            var toDate = $filter('date')($scope.user.toDate, 'yyyy-MM-dd');
            $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&toDate=" + toDate)
                .then(function(response) {
                    $scope.user.noOfdays = Number(response.data.numberOfDays);
                    $scope.halfDay = false;
                    $scope.user.halfDaySlot = 0;
                });
        }
        $scope.to_datePicker = function() {
            var fromDate = $filter('date')($scope.user.fromDate, 'yyyy-MM-dd');
            var days = $scope.user.noOfdays;
            // console.log(fromDate);
            $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&numberOfDays=" + days)
                .then(function(response) {
                    $scope.user.toDate = new Date(response.data.toDate);
                });
        }
        $scope.dayPicker = function() {
            var fromDate = $filter('date')($scope.user.fromDate, 'yyyy-MM-dd');
            var toDate = $filter('date')($scope.user.toDate, 'yyyy-MM-dd');
            $http.get($rootScope.nodeUrl + "date?fromDate=" + fromDate + "&toDate=" + toDate)
                .then(function(response) {
                    $scope.user.noOfdays = Number(response.data.numberOfDays);
                });
        }

        // Update leave
        $scope.leaveUpdate = function(validation, leaveId) {
            if (validation) {
                var data = {
                    fromDate: $scope.user.fromDate,
                    toDate: $scope.user.toDate,
                    numberOfDays: $scope.user.noOfdays,
                    comments: $scope.user.comment,
                    employeeId: "" + $rootScope.globals.currentUser.empId,
                    halfDaySlot: $scope.user.halfDaySlot,
                    approverId: $scope.user.approverId,
                    action: "update",
                    id: leaveId
                };

                $scope.json = angular.toJson(data);
                $http({
                        url: $rootScope.nodeUrl + 'api/leave_requests/' + leaveId,
                        method: "PUT",
                        data: $scope.json
                    })
                    .then(function(response) {
                            // success

                            $http.get($rootScope.nodeUrl + "api/leave_requests/leaveStatistics?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                .then(function(response) {

                                    $rootScope.myLeave = response.data;
                                });
                            $http.get($rootScope.nodeUrl + "api/leave_requests/getList?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                .then(function(response) {
                                    $scope.employee = response.data;
                                    commonData.setHistory(response.data);
                                    dialogMessage.showAlert('Your leave has been updated successfully');
                                    $mdDialog.cancel();
                                });
                            var API_leaveSummary = $rootScope.nodeUrl + "api/leave_requests/leaveHistorySummary?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token;
                            webService.getSummary(API_leaveSummary).success(function(data) {
                                commonData.setEmployeeLeaveSummary(data);
                            });

                        },
                        function(response) { // optional
                            // failed
                            dialogMessage.showAlert("Updation Failed");
                        });
            }
        }
    });
