'use strict';

describe('Controller: NewrequestCtrl', function () {

  // load the controller's module
  beforeEach(module('yoErpLmsApp'));

  var NewrequestCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewrequestCtrl = $controller('NewrequestCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewrequestCtrl.awesomeThings.length).toBe(3);
  });
});
