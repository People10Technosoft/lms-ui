'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:ApprovalsCtrl
 * @description
 * # ApprovalsCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('ApprovalsCtrl', function($scope, $rootScope, $http, $mdDialog, $timeout, colorSelector, leaveAction, apiRequests, commonData, dialogMessage, sweet, $mdToast, $interval, $location, $routeParams, $window, webService) {

        $scope.approverList = [];
        $scope.colour = colorSelector.selectColor();
        $scope.userComment = "";
        $scope.hoverEvent = true;
        $scope.commentLabel = "Comments";
        $scope.checked = [];
        $scope.approveBtn = false;
        $scope.approveAllValue = true;
        $scope.progress = true;
        $scope.noData = false;
        $scope.progress_mode = "indeterminate";
        $scope.showTooltip = false;
        $scope.tooltip = commonData.getTooltip();

        // Open current card after back functionality
        if ($rootScope.e_id != undefined) {
            $scope.selectedUserIndex = $rootScope.index;
            $scope.title = $rootScope.group;
            var eId = $rootScope.e_id;
            var lId = $rootScope.lvId;
            $http.get($rootScope.nodeUrl + "api/leave_workflows/getLeaveWorkflows?leaveId=" + lId + "&employeeId=" + eId + "&access_token=" + $rootScope.globals.currentUser.token)
                .then(function(response) {
                    $scope.workflow = response.data;
                });
            $rootScope.index = undefined;
            $rootScope.group = undefined;
            $rootScope.e_id = undefined;
            $rootScope.lvId = undefined;
        }

        commonData.setScreen("Approvals");

        commonData.observeRequest().then(null, null, function(approvalData) {
            $scope.approverList = approvalData;
            if ($scope.approverList.length === 0) {
                $scope.noData = true;
            } else {
                $scope.noData = false;
            }

        });

        // API for getting leave request waiting for approval
        $http.get($rootScope.nodeUrl + "api/leave_requests/leaveRequestsWaitingForApproval?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
            .then(function(response) {
                // console.log($rootScope.nodeUrl+"api/leave_requests/leaveRequestsWaitingForApproval?id="+$rootScope.globals.currentUser.empId+"&access_token="+$rootScope.globals.currentUser.token);
                commonData.setRequest(response.data);
                // $scope.approverList=commonData.getRequest();
                $scope.progress = false;
                $scope.progress_mode = "null";
            });

        // collapse card view
        $scope.selectUserIndex = function(index, title) {
            if ($scope.selectedUserIndex !== index || $scope.title !== title) {
                $scope.selectedUserIndex = index;
                $scope.title = title;
            } else {
                $scope.selectedUserIndex = undefined;
                $scope.title = undefined;
            }
        };

        // Expand card view
        $scope.leaveDetails = function(event, index, title, leaveId, empId) {
            event.stopPropagation();
            $scope.selectedUserIndex = index;
            $scope.title = title;
            $scope.workflow = undefined;
            $http.get($rootScope.nodeUrl + "api/leave_workflows/getLeaveWorkflows?leaveId=" + leaveId + "&employeeId=" + empId + "&access_token=" + $rootScope.globals.currentUser.token)
                .then(function(response) {
                    $scope.workflow = response.data;
                });
            //New Leave Summary Data
            var API_leaveSummary = $rootScope.nodeUrl + "api/leave_requests/leaveHistorySummary?id=" + empId + "&access_token=" + $rootScope.globals.currentUser.token;
            webService.getSummary(API_leaveSummary).success(function(data) {
                $scope.leaveSummary = data;
            });
        };

        // Open dialog prompt for taking action
        $scope.showPrompt = function(ev, record, action) {
            $scope.data = record;
            $scope.action = action;
            var alert = $mdDialog.alert({
                parent: angular.element(document.body),
                hasBackdrop: false,
                clickOutsideToClose: true,
                disableParentScroll: true,
                targetEvent: ev,
                scope: $scope.$new(),
                template: '<md-dialog class="commentDialog" aria-label="PromptDialog">' +
                    '   <md-toolbar>' +
                    '     <h4 style="margin:auto; color:black;">' + action + ' Action</h4>' +
                    '   </md-toolbar>' +
                    '  <md-content style="overflow:hidden">' +
                    '   <form name="actionForm">' +
                    '   <md-input-container md-no-float style="margin-left:20px;width:87%;">' +
                    '     <label>' +
                    '       Approver comment*' +
                    '     </label>' +
                    '     <input ng-pattern="/[^\s]+/"  maxlength="100" ng-model="userComment1" required>' +
                    '   </md-input-container>' +
                    '   </form>' +
                    '  </md-content>' +
                    '  <md-dialog-actions  style="justify-content:flex-end !important; margin:0% 3% 3% 0%">' +
                    '    <md-button  class="md-raised md-primary" ng-disabled="actionForm.$invalid || approvalValue" ng-click="leaveResponse($event,data,action,userComment1,actionForm.$valid);">' +
                    action +
                    '    </md-button>' +
                    '  </md-dialog-actions>' +
                    '</md-dialog>'
            });
            $mdDialog
                .show(alert)
                .finally(function() {
                    alert = undefined;
                });
        };


        // Taking action for leave request
        $scope.leaveResponse = function(event, data, action, comment, valid) {
            $scope.selectedAll = false;
            if (valid) {
                event.stopPropagation();
                $scope.approvalValue = true;
                var result;
                // var toast = $mdToast.simple()
                //     .textContent('Action processing...')
                //     .action('UNDO')
                //     .highlightAction(false)
                //     .position('bottom left')
                //     .hideDelay(2000);
                // $mdToast.show(toast).then(function(response) {
                //     if (response == 'ok') {
                //         $scope.approvalValue = false;
                //
                //     } else {
                leaveAction.actionTaken(data, action, comment);
                $scope.selectedUserIndex = undefined;
                $scope.approvalValue = false;
                // }
                // });
            } else {
                $scope.commentLabel = "Comment is Required";
                $scope.approvalValue = false;
            }
        }

        // Approve all action
        $scope.approveAll = function() {
            $scope.approveAllValue = false;
            var data = [];
            angular.forEach($scope.approverList, function(obj) {
                angular.forEach(obj.data, function(obj2) {
                    if (obj2.value) {
                        data.push(obj2);
                    }
                })

            });
            var result;
            // var toast = $mdToast.simple()
            //     .textContent('Action Processing...')
            //     .action('UNDO')
            //     .highlightAction(false)
            //     .position('bottom left')
            //     .hideDelay(2000);
            // $mdToast.show(toast).then(function(response) {
            //     if (response == 'ok') {
            //         $scope.approveAllValue = true;
            //
            //     } else {
            var status = leaveAction.actionToAll(data);
            $scope.approveAllValue = true;
            //     }
            // });

        }

        // Select all list
        $scope.selectAll = function() {
            angular.forEach($scope.approverList, function(obj) {
                angular.forEach(obj.data, function(obj2) {
                    obj2.value = $scope.selectedAll;
                });
            });
            $scope.approveBtn = $scope.selectedAll;
        }

        // Un select all
        $scope.unSelectAll = function(value) {
            if (value) {
                $scope.approveBtn = true;
                var flag = 0;
                angular.forEach($scope.approverList, function(obj) {
                    angular.forEach(obj.data, function(obj2) {
                        if (!obj2.value) { //checkbox not selected
                            flag = 1;
                        }
                    });
                });
                if (!flag) { //All checkboxes are selected
                    $scope.selectedAll = true;
                }
            } else if ($scope.selectedAll && !value) {
                $scope.selectedAll = false; //Anyone checkbox not selected
            } else {
                var ApproveFlag = 0;
                angular.forEach($scope.approverList, function(obj) {
                    angular.forEach(obj.data, function(obj2) {
                        if (obj2.value) { //checkbox selected
                            ApproveFlag = 1;
                        }
                    });

                });
                if (!ApproveFlag) {
                    $scope.approveBtn = false; // checkboxes are not selected
                }

            }
        }

        /*Redirect to MySpace view*/

        $scope.memberSpace = function(event, id, index, title, leaveId) {
            event.stopPropagation();
            // console.log(title);
            var url = "/member_space/" + id + "/" + index + "/" + title + "/" + leaveId;
            $location.path(url);
        }


        //Sample Data
        /*
              $scope.approverList=
        [

          {
            "title": "May",
            "data": [
                    {
                "fromEmployee": "1035",
                "employeeOfficialId": "P10E0151",
                "fromDate": "2016-01-04T00:00:00.000Z",
                "toDate": "2016-01-04T00:00:00.000Z",
                "numberOfDays": 1,
                "typeOfLeave": 1,
                "description": "Personal",
                "employeeName": "Muthulakshmi V",
                "leaveId": 3183,
                "designation": "Engineering Manager",
                "leaveType": "Loss of Pay",
                "leaveRemaining": 10,
                "totalLeavesTaken": 8.5,
                "appliedOn": "2016-05-04T07:29:04.000Z",
                "lastLeaveTakenFromDate": "2016-04-22T00:00:00.000Z",
                "lastLeaveTakenToDate": "2016-04-22T00:00:00.000Z"
              },
            ]
          },
          {
            "title": "April",
            "data": [
              {
                "fromEmployee": "1020",
                "employeeOfficialId": "P10E0126",
                "fromDate": "2016-01-22T00:00:00.000Z",
                "toDate": "2016-01-22T00:00:00.000Z",
                "numberOfDays": 1,
                "typeOfLeave": 4,
                "description": "Due to headahe",
                "employeeName": "Raghwendra Kumar",
                "leaveId": 3201,
                "designation": "Software Engineer",
                "leaveType": "Privilege",
                "leaveRemaining": 3,
                "totalLeavesTaken": 2,
                "appliedOn": "2016-04-04T08:10:06.000Z",
                "lastLeaveTakenFromDate": "2016-02-02T00:00:00.000Z",
                "lastLeaveTakenToDate": "2016-02-02T00:00:00.000Z"
              },
              {
                "fromEmployee": "1015",
                "employeeOfficialId": "P10E0118",
                "fromDate": "2016-03-09T00:00:00.000Z",
                "toDate": "2016-03-09T00:00:00.000Z",
                "numberOfDays": 0.5,
                "typeOfLeave": 4,
                "description": "Went to native for emergency",
                "employeeName": "Sibi Joseph",
                "leaveId": 3269,
                "halfDay": 2,
                "designation": "Lead UI UX Designer",
                "leaveType": "Privilege",
                "leaveRemaining": 14,
                "totalLeavesTaken": 1,
                "appliedOn": "2016-04-25T03:46:55.000Z",
                "lastLeaveTakenFromDate": "2016-01-12T00:00:00.000Z",
                "lastLeaveTakenToDate": "2016-01-12T00:00:00.000Z"
              },
              {
                "fromEmployee": "1015",
                "employeeOfficialId": "P10E0118",
                "fromDate": "2016-03-10T00:00:00.000Z",
                "toDate": "2016-03-11T00:00:00.000Z",
                "numberOfDays": 2,
                "typeOfLeave": 4,
                "description": "Went to Native for emergency.",
                "employeeName": "Sibi Joseph",
                "leaveId": 3267,
                "designation": "Lead UI UX Designer",
                "leaveType": "Privilege",
                "leaveRemaining": 14,
                "totalLeavesTaken": 1,
                "appliedOn": "2016-04-25T03:46:55.000Z",
                "lastLeaveTakenFromDate": "2016-01-12T00:00:00.000Z",
                "lastLeaveTakenToDate": "2016-01-12T00:00:00.000Z"
              },
              {
                "fromEmployee": "1015",
                "employeeOfficialId": "P10E0118",
                "fromDate": "2016-04-21T00:00:00.000Z",
                "toDate": "2016-04-21T00:00:00.000Z",
                "numberOfDays": 1,
                "typeOfLeave": 4,
                "description": "Not feeling well, went to hospital",
                "employeeName": "Sibi Joseph",
                "leaveId": 3332,
                "designation": "Lead UI UX Designer",
                "leaveType": "Privilege",
                "leaveRemaining": 14,
                "totalLeavesTaken": 1,
                "appliedOn": "2016-04-25T03:46:55.000Z",
                "lastLeaveTakenFromDate": "2016-01-12T00:00:00.000Z",
                "lastLeaveTakenToDate": "2016-01-12T00:00:00.000Z"
              }
            ]
          },

          {
            "title": "March",
            "data": [
              {
                "fromEmployee": "1019",
                "employeeOfficialId": "P10E0125",
                "fromDate": "2016-02-22T00:00:00.000Z",
                "toDate": "2016-02-22T00:00:00.000Z",
                "numberOfDays": 1,
                "typeOfLeave": 4,
                "description": "Tentative plan.",
                "employeeName": "Laxmi Manvi",
                "leaveId": 3231,
                "designation": "Software Engineer",
                "leaveType": "Privilege",
                "leaveRemaining": 15,
                "totalLeavesTaken": 6,
                "appliedOn": "2016-03-01T23:52:12.000Z",
                "lastLeaveTakenFromDate": "2016-04-07T00:00:00.000Z",
                "lastLeaveTakenToDate": "2016-04-07T00:00:00.000Z"
              }
            ]
          }
        ];*/




        /*
            $scope.approverList=[
            {
            "employeeName":"Sibi Joseph",
            "fromDate": "2016-04-20T18:30:00.000Z",
            "toDate": "2016-04-20T18:30:00.000Z",
            "noOfdays": 1,
            "typeOfLeave": "Privilege",
            "status": "Pending",
            "description":"sick leave"
        	},
            {
            "employeeName":"Vimal Mattathil",
            "fromDate": "2016-03-01T18:30:00.000Z",
            "toDate": "2016-03-03T18:30:00.000Z",
            "noOfdays": 3,
            "typeOfLeave": "Privilege",
            "status": "Approved",
            "description":"Marriage Function"

          },
          {
            "employeeName":"Ashika T",
            "fromDate": "2016-03-01T18:30:00.000Z",
            "toDate": "2016-03-03T18:30:00.000Z",
            "noOfdays": 3,
            "typeOfLeave": "Privilege",
            "status": "Approved",
            "description":"Marriage Function"

          },
          {
            "employeeName":"Nithya Murali",
            "fromDate": "2016-03-01T18:30:00.000Z",
            "toDate": "2016-03-03T18:30:00.000Z",
            "noOfdays": 3,
            "typeOfLeave": "Privilege",
            "status": "Approved",
            "description":"Marriage Function"

          },
          {
          	"employeeName":"Christine John",
            "fromDate": "2016-04-10T18:30:00.000Z",
            "toDate": "2016-04-10T18:30:00.000Z",
            "noOfdays": 1,
            "typeOfLeave": "Privilege",
            "status": "Approved",
            "description":"Marriage Function"
        	}];*/

        $scope.Pending = "#fe9304";
        $scope.Remaining = "#00897B";
        $scope.Approved = "#4caf50";
        $scope.Eligibility = "#01579B";


        $scope.colors = {
            "Pending": "#fe9304",
            "LOP": "#9c27b0",
            "privilege": "#e91e63",
            "Remaining": "#00897B",
            "Approved": "#4caf50",
            "Eligibility": "#01579B",
            "toBeTaken": "#795548",
            "COMPOFF": "#4DD0E1",
            "paternity": "#BA68C8",
            "maternity": "#F06292"
        }

        //
        // $scope.leaveSummary = [{
        //         title: "Eligibility",
        //         data: [{
        //             type: "Privilege",
        //             count: 21
        //         }, {
        //             type: "Maternity",
        //             count: 0
        //         }]
        //     }, {
        //         title: "Approved",
        //         data: [{
        //             type: "Privilege",
        //             count: 10
        //         }, {
        //             type: "Maternity",
        //             count: 0
        //         }, {
        //             type: "Loss of Pay",
        //             count: 4
        //         }]
        //     }, {
        //         title: "Pending",
        //         data: [{
        //             type: "Privilege",
        //             count: 1
        //         }, {
        //             type: "Maternity",
        //             count: 0
        //         }]
        //     }, {
        //         title: "Remaining",
        //         data: [{
        //             type: "Privilege",
        //             count: 10
        //         }, {
        //             type: "Maternity",
        //             count: 0
        //         }]
        //     }
        //
        // ];

    });
