'use strict';

describe('Controller: AdminSpaceCtrl', function () {

  // load the controller's module
  beforeEach(module('yoErpLmsApp'));

  var AdminSpaceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdminSpaceCtrl = $controller('AdminSpaceCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AdminSpaceCtrl.awesomeThings.length).toBe(3);
  });
});
