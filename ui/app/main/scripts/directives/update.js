'use strict';

/**
 * @ngdoc directive
 * @name yoErpLmsApp.directive:update
 * @description
 * # update
 */
angular.module('yoErpLmsApp')
    .directive('update', function() {
        return {
            require: 'ngModel',
            controller: 'UpdateCtrl',
            templateUrl: 'employee/views/update.html',
            scope: {
                updateDetails: "=name"
            }

        };
    });
