var express = require('express');
var app = express();

app.use(express.static(__dirname + '/ui/app'));
app.use('/bower_components', express.static(__dirname + '/ui/bower_components'));


app.get('/', function(req, res) {
    res.sendFile(__dirname + "/ui/app/index.html");
})

app.listen(9000, function() {
    console.log("Listening on 9000....")
})
