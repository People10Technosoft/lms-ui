'use strict';

/**
 * @ngdoc overview
 * @name yoErpLmsApp
 * @description
 * # yoErpLmsApp
 *
 * Main module of the application.
 */
angular
    .module('yoErpLmsApp', [
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngAria',
        'ngMaterial',
        'ng-mfb',
        'ngMdIcons',
        'ui.grid',
        'angular-timeline',
        'materialCalendar',
        'ngStorage',
        'hSweetAlert'
    ])
    .config(function($routeProvider, $mdThemingProvider, $mdDateLocaleProvider, $httpProvider) {
        $mdDateLocaleProvider.formatDate = function(date) {
            return moment(date).format('dddd,MMM Do YYYY');
        };
        $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('orange');

        $mdThemingProvider.theme('indigo')
            .primaryPalette('indigo')
            .accentPalette('pink');

        $mdThemingProvider.theme('lime')
            .primaryPalette('lime')
            .accentPalette('orange')
            .warnPalette('blue');

        $mdThemingProvider.theme('green')
            .primaryPalette('green')
            .accentPalette('purple')
            .warnPalette('orange');

        $mdThemingProvider.alwaysWatchTheme(true);
        /*$httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';*/

        $routeProvider
        /*.when('/', {
          templateUrl: 'others/views/main.html',
          controller: 'MainCtrl',
          controllerAs: 'main'
        })*/
            .when('/about', {
                templateUrl: 'main/views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })
            .when('/apply_leave', {
                templateUrl: 'employee/views/apply_leave.html',
                controller: 'ApplyLeaveCtrl',
                controllerAs: 'applyLeave'
            })
            .when('/leave_statistic', {
                templateUrl: 'employee/views/leave_statistic.html',
                controller: 'LeaveStatisticCtrl',
                controllerAs: 'leaveStatistic'
            })
            .when('/newRequest', {
                templateUrl: 'employee/views/new_request.html',
                controller: 'NewrequestCtrl',
                controllerAs: 'newRequest'
            })
            .when('/leave_history', {
                templateUrl: 'employee/views/leave_history.html',
                controller: 'LeaveHistoryCtrl',
                controllerAs: 'leaveHistory',
                /*resolve: {
                  employeeData: ['getEmployeeDetails', function (getEmployeeDetails) {
                    return getEmployeeDetails;
                  }]
                }*/
            })
            .when('/timeline_workflow', {
                templateUrl: 'main/views/timeline_workflow.html',
                controller: 'TimelineWorkflowCtrl',
                controllerAs: 'timelineWorkflow'
            })
            .when('/leave_update', {
                templateUrl: 'employee/views/leave_update.html',
                controller: 'LeaveUpdateCtrl',
                controllerAs: 'leaveUpdate'
            })
            .when('/approvals/:index?/:title?/:id?/:leaveId?', {
                templateUrl: 'manager/views/approvals.html',
                controller: 'ApprovalsCtrl',
                controllerAs: 'approvals'

            })
            .when('/login', {
                templateUrl: 'login/views/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'login'
            })
            .when('/password_reset', {
                templateUrl: 'login/views/password_reset.html',
                controller: 'PasswordResetCtrl',
                controllerAs: 'passwordReset'
            })
            .when('/forgot_password', {
                templateUrl: 'login/views/forgot_password.html',
                controller: 'ForgotPasswordCtrl',
                controllerAs: 'forgotPassword'
            })
            .when('/reset_password_message', {
                templateUrl: 'login/views/reset_password_message.html',
                controller: 'ForgotPasswordCtrl',
                controllerAs: 'forgotPassword'
            })
            .when('/home', {
                templateUrl: 'others/views/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'home'
            })
            .when('/user_detail', {
                templateUrl: 'main/views/user_detail.html',
                controller: 'UserDetailCtrl',
                controllerAs: 'userDetail'
            })
            .when('/my_team', {
                templateUrl: 'manager/views/my_team.html',
                controller: 'MyTeamCtrl',
                controllerAs: 'myTeam'
            })
            .when('/member_space/:id?/:index?/:title?/:leaveId?', {
                templateUrl: 'manager/views/member_space.html',
                controller: 'MemberSpaceCtrl',
                controllerAs: 'memberSpace'
            })
            .when('/admin_space', {
                templateUrl: 'administration/views/admin_space.html',
                controller: 'AdminSpaceCtrl',
                controllerAs: 'adminSpace'
            })
            .when('/update_leave_eligibility', {
                templateUrl: 'others/views/update_leave_eligibility.html',
                controller: 'UpdateLeaveEligibilityCtrl',
                controllerAs: 'updateLeave'
            })
            .otherwise({
                redirectTo: '/login'
            });
    })
    .run(['$rootScope', '$location', '$http', '$mdDialog', '$interval', '$window', 'commonData', '$routeParams', function($rootScope, $location, $http, $mdDialog, $interval, $window, commonData, $routeParams) {

        if (window.sessionStorage['globals']) {
            $rootScope.globals = JSON.parse(window.sessionStorage['globals']);
        } else {
            $rootScope.globals = {};
        }

        var lastDigestRun = Date.now();
        var idleCheck = $interval(function() {
            var now = Date.now();
            if (now - lastDigestRun > 15 * 60 * 1000) {
                // logout
                $mdDialog.cancel();
                $rootScope.globals = {};
                window.sessionStorage.clear();
                $location.path('/login');

            }
        }, 60 * 1000);

        $rootScope.$on('$routeChangeStart', function(evt) {
            lastDigestRun = Date.now();
        });

        $rootScope.$on('$locationChangeStart', function(event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() === '/forgot_password') {

                $location.path('/forgot_password');
            } else if ($location.path() === '/reset_password_message') {

                $location.path('/reset_password_message');
            } else if ($location.path() === '/password_reset') {

                $location.path('/password_reset');
            } else if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                window.location.href = '#/login';
            } else if ($location.path() === '/login' && $rootScope.globals.currentUser) {
                $location.path('/leave_history');
            } else if ($location.path() !== '/login' && $rootScope.globals.currentUser.freshLogin == 1) {
                window.location.href = '#/password_reset';
            }

        });
        var history = [];
        $rootScope.$on('$locationChangeSuccess', function() {
            history.push($location.$$path);
        });

        $rootScope.back = function() {
            var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
            $location.path(prevUrl);
            if ($location.path() === '/my_team') {
                $rootScope.view = "Approvals";
                commonData.setScreen("MyTeam");

            } else {
                $rootScope.view = "Approvals";
                commonData.setScreen("Approvals");

            }
            history = []; //Delete history array after going back
        };

    }]);
