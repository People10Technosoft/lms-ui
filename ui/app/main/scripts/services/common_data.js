'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.commonData
 * @description
 * # commonData
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
    .service('commonData', function($q) {
        // AngularJS will instantiate a singleton by calling "new" on this function


        // This service sets the providerId & timePeriod values
        // Also has an observer function, that notifies of any change in provider or time period details to the controllers who are observing these two params

        var records = [];
        var approvalRequest = [];
        var actionTaken = false;
        var screen;
        var updateAction = false;
        var leaves = [];
        var workflow = [];
        var history = [];
        var memberLeaves = [];
        var employeeList = [];
        var eligibility = [];
        var addEligibility = [];
        var tabContent;
        var profileInfo;
        var leaveSummary = [];
        var employeeLeaveSummary = [];

        var colors = {
            Pending: "#fe9304",
            Future: "#4caf50",
            Taken: "#bdbdbd"
        };
        var leaveColor = {
            "pending": "#fe9304",
            "LOP": "#9c27b0",
            "privilege": "#e91e63",
            "remaining": "#00897B",
            "totalApproved": "#4caf50",
            "totalLeaves": "#01579B",
            "toBeTaken": "#795548",
            "COMPOFF": "#4DD0E1",
            "paternity": "#BA68C8",
            "maternity": "#F06292"
        };

        var infoColor = {
            "basicInfo": "#8cc474",
            "personalInfo": "#4dbfd9",
            "contactInfo": "#bc5679",
            "permanentContactInfo": "6f85bf",
            "contactNoInfo": "#79b256"
        }

        var dialogColor = {
            "title": undefined,
            "color": undefined,
            "btnColor": undefined
        };

        var tooltip = {
            "Apply": "Applied",
            "Approve": "Approved",
            "Sendback": "Sendback",
            "Cancel": "Cancelled",
            "Deny": "Denied",
            "Update": "Updated",
            "CancelCancellationRequest": "cancelled the cancellation request"
        }

        var defer = $q.defer();
        var defer1 = $q.defer();
        var defer2 = $q.defer();
        var defer3 = $q.defer();
        var defer4 = $q.defer();
        var defer5 = $q.defer();
        var defer6 = $q.defer();
        var defer7 = $q.defer();
        var defer8 = $q.defer();
        var defer9 = $q.defer();
        var defer10 = $q.defer();
        var defer11 = $q.defer();
        var defer12 = $q.defer();
        var defer13 = $q.defer();
        var defer14 = $q.defer();
        var defer15 = $q.defer();


        return {

            observeRecords: function() {
                return defer.promise;
            },
            getRecords: function() {

                return records;
            },
            setRecords: function(updatedData) {
                records = updatedData;
                defer.notify(records);
            },
            setFlag: function(flag) {

                actionTaken = flag;
            },
            getFlag: function() {

                return actionTaken;
            },
            observeRequest: function() {
                return defer1.promise;
            },
            getRequest: function() {

                return approvalRequest;
            },
            setRequest: function(data) {
                approvalRequest = data;
                defer1.notify(approvalRequest);
            },
            observeUpdate: function() {
                return defer2.promise;
            },
            getUpdate: function() {

                return updateAction;
            },
            setUpdate: function(data) {
                updateAction = data;
                defer2.notify(updateAction);
            },
            observeLeaves: function() {
                return defer3.promise;
            },
            getLeaves: function() {

                return leaves;
            },
            setLeaves: function(data) {
                leaves = data;
                defer3.notify(leaves);
            },
            observeWorkflow: function() {
                return defer4.promise;
            },
            getWorkflow: function() {

                return workflow;
            },
            setWorkflow: function(data) {
                workflow = data;
                defer4.notify(workflow);
            },
            getColors: function() {
                return colors;
            },
            observeHistory: function() {
                return defer5.promise;
            },
            getHistory: function() {

                return history;
            },
            setHistory: function(data) {
                history = data;
                defer5.notify(history);
            },
            observeScreen: function() {
                return defer6.promise;
            },
            getScreen: function() {

                return screen;
            },
            setScreen: function(scrn) {
                screen = scrn;
                defer6.notify(screen);
            },
            observeMemberLeave: function() {
                return defer7.promise;
            },
            getMemberLeave: function() {

                return memberLeaves;
            },
            setMemberLeave: function(leave) {
                memberLeaves = leave;
                defer7.notify(memberLeaves);
            },
            getLeaveColor: function() {
                return leaveColor;
            },
            getInfoColors: function() {
                return infoColor;
            },
            observeDialogData: function() {
                return defer8.promise;
            },
            getDialogData: function() {

                return dialogColor;
            },
            setDialogData: function(tle, clr1, clr2) {
                dialogColor.title = tle;
                dialogColor.color = clr1;
                dialogColor.btnColor = clr2;
                defer8.notify(dialogColor);
            },
            observeEmployeeList: function() {
                return defer9.promise;
            },
            getEmployeeList: function() {

                return employeeList;
            },
            setEmployeeList: function(empList) {
                // console.log("djjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjf");
                employeeList = empList
                defer9.notify(employeeList);
            },
            getTooltip: function() {
                return tooltip;
            },
            observeEligibility: function() {
                return defer10.promise;
            },
            getEligibility: function() {

                return eligibility;
            },
            setEligibility: function(elg) {
                eligibility = elg;
                defer10.notify(eligibility);
            },
            observeTabContent: function() {
                return defer11.promise;
            },
            getTabContent: function() {

                return tabContent;
            },
            setTabContent: function(url) {
                // console.log("TAB CONTENT");
                tabContent = url;
                defer11.notify(tabContent);
            },
            observeProfileInfo: function() {
                return defer12.promise;
            },
            getProfileInfo: function() {

                return profileInfo;
            },
            setProfileInfo: function(prfl) {
                profileInfo = prfl;
                defer12.notify(profileInfo);
            },
            observeLeaveSummary: function() {
                return defer13.promise;
            },
            getLeaveSummary: function() {

                return leaveSummary;
            },
            setLeaveSummary: function(summary) {
                leaveSummary = summary;
                defer13.notify(summary);
            },
            observeAddEligibility: function() {
                return defer14.promise;
            },
            getAddEligibility: function() {

                return addEligibility;
            },
            setAddEligibility: function(elg) {
                addEligibility = elg;
                defer14.notify(addEligibility);
            },
            observeEmployeeLeaveSummary: function() {
                return defer15.promise;
            },
            getEmployeeLeaveSummary: function() {

                return employeeLeaveSummary;
            },
            setEmployeeLeaveSummary: function(data) {
                employeeLeaveSummary = data;
                defer15.notify(employeeLeaveSummary);
            }
        };
    });
