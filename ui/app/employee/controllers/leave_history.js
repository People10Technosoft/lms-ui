'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:LeaveHistoryCtrl
 * @description
 * # LeaveHistoryCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('LeaveHistoryCtrl', function($scope, $rootScope, $http, $mdDialog, $mdSidenav, $localStorage, $mdMedia, sort, commonData, dialogMessage, leaveType, sweet, $mdToast, webService) {
        $scope.today = new Date();
        $scope.employee = {};
        $scope.$storage = $localStorage;
        $scope.header = {};
        $scope.updateAction = false;
        $scope.records = [];
        var counter = 0;
        $scope.cancelValue = true;
        $scope.showTooltip = false;
        $scope.cancelRequest = false;
        $scope.tooltip = commonData.getTooltip();

        // API for get leave history
        $http.get($rootScope.nodeUrl + "api/leave_requests/getList?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
            .then(function(response) {
                $scope.history = response.data
                for (var i = 0; i < $scope.history.length; i++) {

                    for (var j = 0; j < $scope.history[i].data.length; j++) {
                        $scope.history[i].data[j].fromDate = new Date($scope.history[i].data[j].fromDate);
                        $scope.history[i].data[j].toDate = new Date($scope.history[i].data[j].toDate);
                    }
                }

            });

        commonData.observeUpdate().then(null, null, function(data) {
            $scope.updateAction = data;
        });
        $scope.updateAction = commonData.getUpdate();
        commonData.observeHistory().then(null, null, function(data) {
            $scope.history = data;
        });
        // Observe leave workflow
        commonData.observeWorkflow().then(null, null, function(data) {
            console.log("workflow");
            $scope.workflow = data;
            var index = $scope.workflow.length - 1;
            $rootScope.employeeApprover = $scope.workflow[index].toEmployee;
        });

        // Expand card with workflow
        $scope.leaveDetails = function(event, index, title, leaveId) {
            event.stopPropagation();
            $scope.selectedUserIndex = index;
            $scope.title = title;
            $http.get($rootScope.nodeUrl + "api/leave_workflows/getLeaveWorkflows?leaveId=" + leaveId + "&employeeId=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                .then(function(response) {
                    $scope.workflow = response.data;
                    var index = $scope.workflow.length - 1;
                    $rootScope.employeeApprover = $scope.workflow[index].toEmployee;
                });
        };

        // Collapse the card
        $scope.listItemClick = function($index) {
            var clickedItem = $scope.items[$index];
        };
        $scope.selectedUserIndex = undefined;
        $scope.title = undefined;
        $scope.selectUserIndex = function(index, title) {
            if ($scope.selectedUserIndex !== index || $scope.title !== title) {
                $scope.selectedUserIndex = index;
                $scope.title = title;
            } else {
                $scope.selectedUserIndex = undefined;
                $scope.title = undefined;
            }
        };

        //expand card for Update Leave
        $scope.updateLeave = function(message, event) {
            event.stopPropagation();
            commonData.setUpdate(true);
        }

        //Open dialog for Cancel Leave request
        $scope.showPrompt = function(ev, record) {
            $scope.record = record;
            var parentEl = angular.element(document.querySelector('md-content'));
            $mdDialog.show({
                parent: angular.element(document.body),
                hasBackdrop: false,
                clickOutsideToClose: true,
                disableParentScroll: true,
                targetEvent: ev,
                scope: $scope.$new(),
                template: '<md-dialog class="commentDialog" aria-label="PromptDialog">' +
                    '   <md-toolbar>' +
                    '     <h4 style="margin:auto; color:black;">Cancel your Leave</h4>' +
                    '   </md-toolbar>' +
                    '  <md-content style="overflow:hidden">' +
                    '   <form name="cancelForm">' +
                    '   <md-input-container md-no-float style="margin-left:20px;width:87%;">' +
                    '     <label>' +
                    '       comments*' +
                    '     </label>' +
                    '     <input ng-pattern="/[^\s]+/"  maxlength="100" ng-model="cancelComments" required>' +
                    '   </md-input-container>' +
                    '   </form>' +
                    '  </md-content>' +
                    '  <md-dialog-actions style="justify-content:flex-end !important; margin:0% 3% 3% 0%">' +
                    '    <md-button  class="md-raised md-primary" ng-disabled="cancelForm.$invalid || cancelRequest" ng-click="cancelLeave(record.leaveRequestId, record.typeOfLeave,record.fromDate, record.toDate,record.numberOfdays, $event, record.status,cancelComments);">' +
                    '      Cancel Leave' +
                    '    </md-button>' +
                    '  </md-dialog-actions>' +
                    '</md-dialog>'

            });
        };

        // Cancel leave request
        $scope.cancelLeave = function(leaveID, type, fDate, tDate, days, event, status, comment) {
            event.stopPropagation();
            $scope.cancelValue = false;
            $scope.cancelRequest = true;
            var result;
            var toast = $mdToast.simple()
                .textContent('Cancelling...')
                .action('UNDO')
                .highlightAction(false)
                .position('bottom left')
                .hideDelay(2000);


            $mdToast.show(toast).then(function(response) {
                if (response == 'ok') {
                    $scope.cancelValue = true;
                    $scope.cancelRequest = false;
                } else {
                    $scope.cancelRequest = false;
                    var Data = {
                        "action": "cancel",
                        "type": type,
                        "id": leaveID,
                        "employeeId": $rootScope.globals.currentUser.empId,
                        "fromDate": fDate,
                        "toDate": tDate,
                        "numberOfDays": days,
                        "currentStatus": status,
                        "comments": comment
                    };
                    $scope.json = angular.toJson(Data);
                    $http({
                            url: $rootScope.nodeUrl + "api/leave_requests/cancelLeaveRequest?access_token=" + $rootScope.globals.currentUser.token,
                            method: "POST",
                            data: $scope.json
                        })
                        .then(function(response) {
                                // success
                                $scope.cancelValue = true;
                                $mdDialog.hide();
                                dialogMessage.showErrorToast('Your leave has been cancelled successfully');
                                $http.get($rootScope.nodeUrl + "api/leave_requests/leaveStatistics?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                    .then(function(response) {
                                        $rootScope.myLeave = response.data; //Update leave Summary
                                    });
                                $http.get($rootScope.nodeUrl + "specialLeaves?id=" + $rootScope.globals.currentUser.empId)
                                    .then(function(response) {
                                        commonData.setLeaves(leaveType.leaveItems(response.data));
                                    });
                                $http.get($rootScope.nodeUrl + "api/leave_requests/getList?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                                    .then(function(response) {
                                        commonData.setHistory(response.data); //Update leave history
                                        $scope.selectedUserIndex = undefined;
                                    });
                                var API_leaveSummary = $rootScope.nodeUrl + "api/leave_requests/leaveHistorySummary?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token;
                                webService.getSummary(API_leaveSummary).success(function(data) {
                                    commonData.setEmployeeLeaveSummary(data);
                                });



                            },
                            function(response) { // optional
                                // failed
                                // console.log("failed");
                                dialogMessage.showErrorToast(response.data.error.message);
                                $scope.cancelValue = true;
                            });
                }
            }, function() {
                dialogMessage.showAlert('Your leave has not been cancelled');
            });


        }

        $scope.toggleMenu = function() {
            $mdSidenav('left').toggle();
        };



        $scope.listItemClick = function($index) {
            var clickedItem = $scope.items[$index];
        };
        $scope.selectedUserIndex = undefined;
        $scope.selectUserIndex = function(index) {
            if ($scope.selectedUserIndex !== index) {
                $scope.selectedUserIndex = index;
            } else {
                $scope.selectedUserIndex = undefined;
            }
        };
    });
