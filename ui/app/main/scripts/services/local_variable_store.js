'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.localVariableStore
 * @description
 * # localVariableStore
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
    .service('localVariableStore', function() {
        // AngularJS will instantiate a singleton by calling "new" on this function
        angular.element($window).on('storage', function(event) {
            if (event.key === 'my-storage') {
                $rootScope.$apply();
            }
        });
        return {
            setData: function(val) {
                $window.localStorage && $window.localStorage.setItem('my-storage', val);
                return this;
            },
            getData: function() {
                return $window.localStorage && $window.localStorage.getItem('my-storage');
            }
        };
    });
