'use strict';

/**
 * @ngdoc service
 * @name yoErpLmsApp.apiRequests
 * @description
 * # apiRequests
 * Service in the yoErpLmsApp.
 */
angular.module('yoErpLmsApp')
    .service('apiRequests', function($rootScope, $http) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        this.arrovalsRequestList = function() {
            $http.get($rootScope.nodeUrl + "api/leave_requests/leaveRequestsWaitingForApproval?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
                .then(function(response) {
                    var data = response.data;
                    return data;
                });
        }
    });
