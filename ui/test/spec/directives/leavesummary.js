'use strict';

describe('Directive: leaveSummary', function () {

  // load the directive's module
  beforeEach(module('yoErpLmsApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<leave-summary></leave-summary>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the leaveSummary directive');
  }));
});
