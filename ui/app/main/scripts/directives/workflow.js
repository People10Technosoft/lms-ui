'use strict';

/**
 * @ngdoc directive
 * @name yoErpLmsApp.directive:workFlow
 * @description
 * # workFlow
 */
angular.module('yoErpLmsApp')
    .directive('workFlow', function() {
        return {
            controller: 'LeaveHistoryCtrl',
            templateUrl: 'main/views/timeline_workflow.html',
            scope: {
                workflow: "=name"
            },
            restrict: 'EA'
        };
    });
