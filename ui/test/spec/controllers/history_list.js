'use strict';

describe('Controller: HistoryListCtrl', function () {

  // load the controller's module
  beforeEach(module('yoErpLmsApp'));

  var HistoryListCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    HistoryListCtrl = $controller('HistoryListCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(HistoryListCtrl.awesomeThings.length).toBe(3);
  });
});
