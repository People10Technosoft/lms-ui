'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('HomeCtrl', function() {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
    });
