'use strict';

describe('Service: commonData', function () {

  // load the service's module
  beforeEach(module('yoErpLmsApp'));

  // instantiate service
  var commonData;
  beforeEach(inject(function (_commonData_) {
    commonData = _commonData_;
  }));

  it('should do something', function () {
    expect(!!commonData).toBe(true);
  });

});
