'use strict';

describe('Service: dateUtility', function () {

  // load the service's module
  beforeEach(module('yoErpLmsApp'));

  // instantiate service
  var dateUtility;
  beforeEach(inject(function (_dateUtility_) {
    dateUtility = _dateUtility_;
  }));

  it('should do something', function () {
    expect(!!dateUtility).toBe(true);
  });

});
