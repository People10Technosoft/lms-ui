'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:ForgotPasswordCtrl
 * @description
 * # ForgotPasswordCtrl
 * Controller of the yoErpLmsApp
 */

// Not in use
angular.module('yoErpLmsApp')
    .controller('ForgotPasswordCtrl', ['$scope', '$location', '$http', '$rootScope', 'sweet', function($scope, $location, $http, $rootScope, sweet) {
        $scope.reset_password = function(email) {
            var data = {
                email: email
            }
            $scope.json = angular.toJson(data);
            $http({
                    url: $rootScope.nodeUrl + 'api/employees/reset',
                    method: "POST",
                    data: $scope.json
                })
                .then(function(response) {
                        window.location.href = '/#/reset_password_message';
                    },
                    function(response) { // optional
                        // failed
                        dialogMessage.showErrorToast(response.data.error.message);
                    });

        }
    }]);
