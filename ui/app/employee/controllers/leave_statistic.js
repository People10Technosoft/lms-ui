'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:LeaveStatisticCtrl
 * @description
 * # LeaveStatisticCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('LeaveStatisticCtrl', function($scope, $rootScope, $http, $localStorage) {

        // API for leave summary
        $http.get($rootScope.nodeUrl + "api/leave_requests/leaveStatistics?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
            .then(function(response) {
                $rootScope.myLeave = response.data;
            });

        // Set default color
        $scope.colors = {
            "pending": "#fe9304",
            "LOP": "#9c27b0",
            "privilege": "#e91e63",
            "remaining": "#00897B",
            "totalApproved": "#4caf50",
            "totalLeaves": "#01579B",
            "toBeTaken": "#795548",
            "COMPOFF": "#4DD0E1",
            "paternity": "#BA68C8",
            "maternity": "#F06292"
        }

    });
