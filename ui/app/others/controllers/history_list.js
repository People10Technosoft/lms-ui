'use strict';

/**
 * @ngdoc function
 * @name yoErpLmsApp.controller:HistoryListCtrl
 * @description
 * # HistoryListCtrl
 * Controller of the yoErpLmsApp
 */
angular.module('yoErpLmsApp')
    .controller('HistoryListCtrl', function($scope, $http, $mdDialog, $rootScope, commonData) {
        $scope.colors = commonData.getColors();
        $scope.Taken = "#bdbdbd";
        // console.log($scope.colors);
        $http.get($rootScope.nodeUrl + "api/leave_requests/getList?id=" + $rootScope.globals.currentUser.empId + "&access_token=" + $rootScope.globals.currentUser.token)
            .then(function(response) {
                // console.log("reading...");
                // console.log(response.data);
                $scope.history = response.data;
            });

        $scope.leaveDetails = function(index, title, leaveId) {
            $scope.selectedUserIndex = index;
            $scope.title = title;
            $http.get($rootScope.nodeUrl + "api/leave_workflows/getLeaveWorkflows?id=" + leaveId)
                .then(function(response) {
                    $scope.workflow = response.data;
                });
        };

        $scope.listItemClick = function($index) {
            var clickedItem = $scope.items[$index];
        };
        $scope.selectedUserIndex = undefined;
        $scope.title = undefined;
        $scope.selectUserIndex = function(index, title) {
            if ($scope.selectedUserIndex !== index || $scope.title !== title) {
                $scope.selectedUserIndex = index;
                $scope.title = title;
            } else {
                $scope.selectedUserIndex = undefined;
                $scope.title = undefined;
            }
        };


        /*
                $scope.users = [
            {
              name: { first: 'try', last:'try' }
            },
            {
              name: { first: 'try2', last:'try2' }
            },
            {
              name: { first: 'try3', last:'try3' }
            }];
        */
        /*$scope.myData = [
            {
                "firstName": "Cox",
                "lastName": "Carney",
                "firstName1": "Cox",
                "lastName4": "Carney",
                "status":"pending"
            },
            {
                "firstName": "aaaaaaaaaaaaaaa",
                "lastName": "Carneyaaaaaaaaaa",
                "firstName1": "Coxaaaaaaaaaaaaaaa",
                "lastName4": "Carneyaaaaaaaaaaaa",
                "status":"pending"
            },
            {
                "firstName": "Coxbbbbbbbbbbbbbb",
                "lastName": "Carneybbbbbbbbb",
                "firstName1": "Coxbbbbbbbbbbb",
                "lastName4": "Carney",
                "status":"pending"
            },
            {
                "firstName": "Covvvvvvvvvvvvvvvvx",
                "lastName": "Carneyvvvvvvvvvvv",
                "firstName1": "Coxvvvvvvvvvv",
                "lastName4": "Carney",
                "status":"pending"
            }
        ]*/
    });
